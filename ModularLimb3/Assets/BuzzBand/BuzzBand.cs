// Author: Avinash Sharma, JHU
//         Edited By: Christopher Hunt <chunt11@jhmi.edu>
using UnityEngine;
using UnityEngine.UI;
using System.IO.Ports;


public class BuzzBand : MonoBehaviour
{
    public GraspingLogic GraspLogic;
    // private SerialPort stream = new SerialPort("COM6", 9600);
    private GameObject hand = null;

    public GameObject cylinder = null;
    public Text hud;

    void Start() {
        hand = GameObject.Find("rPalm");
        cylinder = GameObject.Find("Cylinder");
       // stream.Open();
    }
	
	void Update() {
        cylinder = GameObject.Find("Cylinder");

        if (!GraspLogic.Grasping)
        {
            Vector3 position = hand.transform.position - cylinder.transform.position;
            // Debug.Log( string.Format( "Relative Hand Position: [{0}, {1}, {2}]", position.x, position.y, position.z ) );
            if (position.x < 0 && position.y > 0)
            {
            //    stream.WriteLine("UpLeft");
                hud.text = string.Format( "Buzzband: UpLeft" );
            }
            else if (position.x > 0 && position.y > 0)
            {
              //  stream.WriteLine("UpRight");
                hud.text = string.Format("Buzzband: UpRight");
            }
            else if (position.y < 0 && position.x < 0)
            {
              //  stream.WriteLine("DownLeft");
                hud.text = string.Format("Buzzband: DownLeft");
            }
            else if (position.y < 0 && position.x > 0)
            {
                //stream.WriteLine("DownRight");
                hud.text = string.Format("Buzzband: DownRight");
            }
        } else
        {
            hud.text = "Buzzband: Acquired";
        }
    }

    private void OnApplicationQuit() {
        //stream.Close();
    }
}