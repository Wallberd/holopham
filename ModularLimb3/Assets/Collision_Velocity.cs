﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VibrationType = Thalmic.Myo.VibrationType;

public class Collision_Velocity : MonoBehaviour
{
    public GameObject myo_upperarm = null;
    private bool vibrated = false;
    private int numObjects = 0;
    // Use this for initialization
    void Start()
    {
        myo_upperarm = GameObject.Find("Myo1");

    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnCollisionEnter(Collision col)
    {
        ThalmicMyo myo = myo_upperarm.GetComponent<ThalmicMyo>();
        if (!vibrated && col.collider.name == "Cylinder_Prefab"){
            numObjects++;
            if (!vibrated)
            {
                myo.Vibrate(VibrationType.Short);
                vibrated = true;
            }
        }
    }

    void OnCollisionExit(Collision col)
    {
        numObjects--;
        if (numObjects == 0)
        {
            vibrated = false;
        }
    }
}