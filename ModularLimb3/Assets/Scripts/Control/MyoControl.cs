﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// Myo Armband imports
using Pose = Thalmic.Myo.Pose;
using VibrationType = Thalmic.Myo.VibrationType;

public class MyoControl : MonoBehaviour {
    private const int NUM_MPL_JOINT_ANGLES = 7;
    private UDPListener listener;

    public GameObject vmplArm = null;
    public GameObject NetworkObj = null;


    // vMPL variables
    private vMPLMovementArbiter arbiter = null;
    bool update_reference = true;
    private float [] joint_angles = new float[NUM_MPL_JOINT_ANGLES];

    // task variables
    public GameObject cylinder = null;
    public GraspingLogic GraspLogic;

    // text variables
    public Text hud;

	// Use this for initialization
	void Start () {
        Debug.Log("Initializing vMPL variables...");
        arbiter = GameObject.Find("vMPLMovementArbiter").GetComponent<vMPLMovementArbiter>();
        cylinder = GameObject.Find("Cylinder");

        Debug.Log("Initialization complete!");
        listener = NetworkObj.GetComponent<UDPListener>();
    }

    // Update is called once per frame
    void Update() {
        // Flex angle
        joint_angles[0] = listener.get_shoulder_flex();
        // Abduction angle
        joint_angles[1] = 0;//-listener.get_shoulder_abduction();
        // Humeral angle
        joint_angles[2] = 0;
        // Elbow angle
        joint_angles[3] = listener.get_elbow_angle();

        // Wave out rotates counter clockwise
        if (listener.get_pose() == 3)
        {
            joint_angles[4] = joint_angles[4] - 0.75f;
        }
        // Wave in rotates counter clockwise
        else if (listener.get_pose() == 4)
        {
            joint_angles[4] = joint_angles[4] + 0.75f;
        }

        arbiter.SetRightUpperArmAngles(joint_angles);

        // If grasping, make grab motion
        if (listener.get_pose() == 1)
        {
            arbiter.SetMovementState(vMPLMovementArbiter.MOVEMENT_STATE_CYLINDER_GRASP);
        }

        // Spread fingers to release
        else if (listener.get_pose() == 2)
        {
            arbiter.SetMovementState(vMPLMovementArbiter.MOVEMENT_STATE_WAVE);
            GraspLogic.Grasping = false;

        }
    }
}
