﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// Myo Armband imports
using Pose = Thalmic.Myo.Pose;
using VibrationType = Thalmic.Myo.VibrationType;

public class MyoControlOld : MonoBehaviour {
    private const int NUM_MPL_JOINT_ANGLES = 7;

    // Myo Armband variables
    public GameObject myo_forearm = null; //  GameObject.Find("Myo1");
    public GameObject myo_upperarm = null; // GameObject.Find("Myo2");
    public GameObject vmplArm = null;
    public GameObject networkObj = null;

    private Quaternion forearm_reference = Quaternion.identity;
    private Quaternion upperarm_reference = Quaternion.identity;
    private Quaternion upperarm_reference_forward = Quaternion.identity;
    private Quaternion forearm_reference_forward = Quaternion.identity;


    private int refCount;
    private Matrix4x4 fREF = Matrix4x4.identity;
    private Matrix4x4 fREF1 = Matrix4x4.identity;
	private Matrix4x4 fREF2 = Matrix4x4.identity;

    private Vector3 reference_forward = Vector3.zero;
    private Vector3 reference_right = Vector3.zero;
    private Vector3 reference_down = Vector3.zero;

    private float _referenceRoll;

    private Matrix4x4 orthox = Matrix4x4.Rotate(Quaternion.Euler(90.0f, 0.0f, 0.0f));
    private Matrix4x4 orthoy = Matrix4x4.Rotate(Quaternion.Euler(0.0f, 90.0f, 0.0f));

    public ThalmicMyo thalmic_myo_forearm = null;
    public ThalmicMyo thalmic_myo_upperarm = null;


    // vMPL variables
    private vMPLMovementArbiter arbiter = null;
    bool update_reference = true;
    private float [] joint_angles = new float[NUM_MPL_JOINT_ANGLES];

    // task variables
    public GameObject cylinder = null;
    public GraspingLogic GraspLogic;

    // text variables
    public Text hud;

    // Methods for joint angle computations
    // 1 -- in-built euler angle computations
    // 2 -- self-written euler angle computations
    // 3 -- swing-twist decomposition
    // 4 -- yost imu formalisation (error)
    // 5 -- hybrid scheme
    private int method = 8;

	// Use this for initialization
	void Start () {
        Debug.Log("Initializing Myo interfaces...");
        myo_forearm = GameObject.Find("Myo2");
        myo_upperarm = GameObject.Find("Myo1");

        Debug.Log("Initializing Thalmic Myos...");
        thalmic_myo_forearm = myo_forearm.GetComponent<ThalmicMyo>();
        thalmic_myo_upperarm = myo_upperarm.GetComponent<ThalmicMyo>();

        Debug.Log("Initializing vMPL variables...");
        arbiter = GameObject.Find("vMPLMovementArbiter").GetComponent<vMPLMovementArbiter>();

        cylinder = GameObject.Find("Cylinder");

        Debug.Log("Initialization complete!");
    }

    // Update is called once per frame
    void Update() {
        // update references
        if (upperarm_reference.Equals(Quaternion.identity)) {
            update_reference = true;
        }
        if (Input.GetKeyDown("r")) {
            Debug.Log("Updating reference...");
            update_reference = true;
            if (refCount == 2)
                refCount = 0;
        }

        if (update_reference) {
            //Set the reference for the arm straight down
            if (refCount == 0)
            {
                upperarm_reference = myo_upperarm.transform.rotation;
                forearm_reference = myo_forearm.transform.rotation;
                GameObject.Find("ReferenceDown").transform.rotation = upperarm_reference;

                reference_forward = myo_upperarm.transform.forward;
                reference_right = myo_upperarm.transform.right;
                reference_down = -1.0f * myo_upperarm.transform.up;
                refCount++;
            }
            //Set the reference for the arm forward
            else if (refCount == 1) {
                upperarm_reference_forward = myo_upperarm.transform.rotation;
                forearm_reference_forward = myo_forearm.transform.rotation;
                GameObject.Find("ReferenceForward").transform.rotation = upperarm_reference_forward;
                Vector3 referenceZeroRoll = computeZeroRollVector(myo_upperarm.transform.forward);
                _referenceRoll = rollFromZero(referenceZeroRoll, myo_upperarm.transform.forward, myo_upperarm.transform.up);

                refCount++;
            }

            update_reference = false;
        }
        if (refCount == 2){

            ThalmicMyo thalmicMyo = myo_forearm.GetComponent<ThalmicMyo>();
            Quaternion angles = Quaternion.Inverse(upperarm_reference_forward) * myo_upperarm.transform.rotation;
            Quaternion anglesfor = Quaternion.Inverse(forearm_reference_forward) * myo_forearm.transform.rotation;

            GameObject.Find("ReferenceObj").transform.rotation = angles;
            Vector3 forearm_forward = GameObject.Find("ReferenceObj").transform.transform.forward;

            GameObject.Find("ReferenceObj").transform.rotation = anglesfor;
            Vector3 upperarm_forward = GameObject.Find("ReferenceObj").transform.transform.forward;

            float elbow_angle = Vector3.Angle(forearm_forward, upperarm_forward);//newF2.rotation.eulerAngles;

            //Get the third axis of the reference with respect to the user
            Vector3 referenceRight = Vector3.Cross(GameObject.Find("ReferenceDown").transform.forward, GameObject.Find("ReferenceForward").transform.forward);

            //Project the forward vector of the myoband onto the reference plane 
            Vector3 planeVector = Vector3.ProjectOnPlane(myo_upperarm.transform.forward,referenceRight);

            //Project the forward vector of the myoband onto the reference plane 
            Vector3 planeVector2 = Vector3.ProjectOnPlane(myo_upperarm.transform.forward, GameObject.Find("ReferenceDown").transform.forward);

            Vector3 planeVector3 = Vector3.ProjectOnPlane(myo_upperarm.transform.forward, GameObject.Find("ReferenceForward").transform.forward);

            //Calculte the angle between the projection and the downwards reference point. This estimates the shoulder forward swing
            float yangle = Vector3.SignedAngle(GameObject.Find("ReferenceDown").transform.forward, planeVector, referenceRight);

            float xangle;
            //Calculate the angle for the shoulder flex. The max is used to resolve the joint position
            xangle = Mathf.Max(Vector3.SignedAngle(planeVector3, GameObject.Find("ReferenceDown").transform.forward, GameObject.Find("ReferenceForward").transform.forward), -Vector3.SignedAngle(planeVector2, GameObject.Find("ReferenceForward").transform.forward, GameObject.Find("ReferenceDown").transform.forward));

            //Relative roll calculations adapted from JointOrientation.cs
            Vector3 zeroRoll = computeZeroRollVector(myo_upperarm.transform.forward);
            float roll = rollFromZero(zeroRoll, myo_upperarm.transform.forward, myo_upperarm.transform.up);
            float relativeRoll = normalizeAngle(roll - _referenceRoll);
            float zangle = relativeRoll;

            //Projectection adjustment to prevent things from going crazy if the y-axis is aligned too close. 
            if (planeVector.magnitude > 0.2f)
                joint_angles[0] = yangle;

            joint_angles[1] = xangle;
            joint_angles[2] = zangle;
            joint_angles[3] = elbow_angle;

            if (thalmic_myo_forearm.pose == Pose.WaveOut)
            {
                joint_angles[4] = joint_angles[4] - 0.75f;
                // Debug.Log(joint_angles[4]);
            }
            else if (thalmic_myo_forearm.pose == Pose.WaveIn)
            {
                joint_angles[4] = joint_angles[4] + 0.75f;
                //Debug.Log(joint_angles[4]);
            }

            //Debug.Log (angles1);
            //Debug.Log (angles2);

            //hud.text = string.Format("JointAngles: [{0}, {1}, {2}, {3}, {4}]",

        }
        //Debug.Log(string.Format("Sending joint angles: ({0}, {1}, {2}, {3}, {4}, {5}, {6})...\n",
        //                           joint_angles[0], joint_angles[1], joint_angles[2], joint_angles[3],
        //                           joint_angles[4], joint_angles[5], joint_angles[6]));
        arbiter.SetRightUpperArmAngles(joint_angles);

        // if grasping
        if (thalmic_myo_forearm.pose == Pose.Fist) {
            // Debug.Log("Closing hand...");
            arbiter.SetMovementState(vMPLMovementArbiter.MOVEMENT_STATE_CYLINDER_GRASP);
        } else if (thalmic_myo_forearm.pose == Pose.FingersSpread ) {
            // Debug.Log("Opening hand...");
            arbiter.SetMovementState(vMPLMovementArbiter.MOVEMENT_STATE_WAVE);
            GraspLogic.Grasping = false;

        }
        else {
            arbiter.SetMovementState(vMPLMovementArbiter.MOVEMENT_STATE_STOP);
        }
	}

    float FindQuaternionTwist( Quaternion q, Vector3 axis ) {
        // normalize inputs
        q.Normalize();
        axis.Normalize();

        Vector3 ra = new Vector3(q.x, q.y, q.z);
        Vector3 p = Vector3.Dot(ra, axis) * axis; ;
        Quaternion twist = new Quaternion(p.x,p.y,p.z, q.w);
        twist.Normalize();

        float angle;
        Vector3 dir;

        twist.ToAngleAxis(out angle, out dir);
        return angle;
    }

    Vector3 FindOrthonormal(Vector3 normal) {
        Vector3 w = orthox.MultiplyVector( normal ); // Vector3.Transform(normal, orthox);
        float dot = Vector3.Dot(normal, w);
        if ( Mathf.Abs(dot) > 0.6f) {
            w = orthoy.MultiplyVector(normal); // Vector3.Transform(normal, orthoy);
        }
        w.Normalize();

        Vector3 ortho = Vector3.Cross(normal, w);
        ortho.Normalize();
        return ortho;
    }

    Vector3 FindQuaternionEuler( Quaternion q ) {
        float ysqr = q.y * q.y;

        float t0 = 2.0f * (q.w * q.x + q.y * q.z);
        float t1 = 1.0f - 2.0f * (q.x * q.x + ysqr);
        float x = Mathf.Rad2Deg * Mathf.Atan2(t0, t1);

        float t2 = 2.0f * (q.w * q.y - q.z * q.x);
        if ( t2 > 1.0f ) {
            t2 = 1.0f;
        } else if ( t2 < -1.0f ) {
            t2 = -1.0f;
        }
        float y = Mathf.Rad2Deg * Mathf.Asin(t2);

        float t3 = 2.0f * (q.w * q.z + q.x * q.y);
        float t4 = 1.0f - 2.0f * (ysqr + q.z * q.z);
        float z = Mathf.Rad2Deg * Mathf.Atan2(t3, t4);

        return new Vector3(x, y, z);
    }

    // Compute the angle of rotation clockwise about the forward axis relative to the provided zero roll direction.
    // As the armband is rotated about the forward axis this value will change, regardless of which way the
    // forward vector of the Myo is pointing. The returned value will be between -180 and 180 degrees.
    float rollFromZero(Vector3 zeroRoll, Vector3 forward, Vector3 up)
    {
        // The cosine of the angle between the up vector and the zero roll vector. Since both are
        // orthogonal to the forward vector, this tells us how far the Myo has been turned around the
        // forward axis relative to the zero roll vector, but we need to determine separately whether the
        // Myo has been rolled clockwise or counterclockwise.
        float cosine = Vector3.Dot(up, zeroRoll);

        // To determine the sign of the roll, we take the cross product of the up vector and the zero
        // roll vector. This cross product will either be the same or opposite direction as the forward
        // vector depending on whether up is clockwise or counter-clockwise from zero roll.
        // Thus the sign of the dot product of forward and it yields the sign of our roll value.
        Vector3 cp = Vector3.Cross(up, zeroRoll);
        float directionCosine = Vector3.Dot(forward, cp);
        float sign = directionCosine < 0.0f ? 1.0f : -1.0f;

        // Return the angle of roll (in degrees) from the cosine and the sign.
        return sign * Mathf.Rad2Deg * Mathf.Acos(cosine);
    }

    // Compute a vector that points perpendicular to the forward direction,
    // minimizing angular distance from world up (positive Y axis).
    // This represents the direction of no rotation about its forward axis.
    Vector3 computeZeroRollVector(Vector3 forward)
    {
        Vector3 antigravity = Vector3.up;
        Vector3 m = Vector3.Cross(myo_upperarm.transform.forward, antigravity);
        Vector3 roll = Vector3.Cross(m, myo_upperarm.transform.forward);

        return roll.normalized;
    }

    // Adjust the provided angle to be within a -180 to 180.
    float normalizeAngle(float angle)
    {
        if (angle > 180.0f)
        {
            return angle - 360.0f;
        }
        if (angle < -180.0f)
        {
            return angle + 360.0f;
        }
        return angle;
    }


}
