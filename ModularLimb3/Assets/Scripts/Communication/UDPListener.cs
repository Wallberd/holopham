﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;

#if UNITY_EDITOR
using System.Net.Sockets;    
#endif

public class UDPListener : MonoBehaviour
{

#if UNITY_EDITOR
    private float sFlex, sAbduct, elbow;
    private int pose = 10;
    private UdpClient u;
    
    // Start is called before the first frame update
    void Start()
    {
        u = new UdpClient(65534);

    }

    // Update is called once per frame
    async void Update()
    {
        var receivedResults = await u.ReceiveAsync();
        byte[] receiveBytes = receivedResults.Buffer;
        sFlex = System.BitConverter.ToSingle(receiveBytes, 0);
        sAbduct = System.BitConverter.ToSingle(receiveBytes, 4);
        elbow = System.BitConverter.ToSingle(receiveBytes, 8);
        pose = System.BitConverter.ToInt32(receiveBytes, 12);

        Debug.Log(sFlex + " | " + sAbduct + " | " + elbow);
    }

    public float get_shoulder_flex()
    {
        return sFlex;
    }

    public float get_shoulder_abduction()
    {
        return sAbduct;
    }

    public float get_elbow_angle()
    {
        return elbow;
    }

    public int get_pose()
    {
        return pose;
    }

#endif
}
