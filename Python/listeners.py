import myo

# Listener for obtaining Myo data
# Members:
#   myos - Dictionary of Myo identifiers mapped to a 3-element list of the
#          orientation data (index 0), the accelerometer data (index 1), and
#          pose (index 3)
#   myo_names: Dictionary of Myo names mapped to the Myo's unique identifier
#   forearm_myo: Name of Myo on the forearm
#   shoulder_myo: Name of Myo on the shoulder
class listener(myo.DeviceListener):
    def __init__(self, forearm_myo = "ThakorLab2", shoulder_myo = "Thakor Lab #5"):
        self.__myos = {}
        self.__myo_names = {}
        self.__forearm_myo = forearm_myo
        self.__shoulder_myo = shoulder_myo
        super(listener, self).__init__()

    # Adds a Myo connected to the Myo Armband Manager to the dictionary
    # Parameters:
    #   event: The update event handled by the myo library
    def on_paired(self, event):
        print(f"Device {event.device_name} connected!")
        self.__myo_names[event.device_name] = event.device.handle
        self.__myos[event.device.handle] = [None, None, None]

    # Disconnects and removes a Myo from the dictionary
    # Parameters:
    #   event: The update event handled by the myo library
    def on_unpaired(self, event):
        print(f"Device {event.device.handle} disconnected!")

    # Updates the Myo's orientation, acceleration data, and pose
    # Parameters:
    #   event: The update event handled by the myo library
    def on_orientation(self, event):
        self.__myos[event.device.handle][0] = event.orientation
        self.__myos[event.device.handle][1] = event.acceleration

    # Updates the Myo's orientation, acceleration data, and pose
    # Parameters:
    #   event: The update event handled by the myo library
    def on_pose(self, event):
        self.__myos[event.device.handle][2] = event.pose

    # Gets the gyroscope data of the Myo band
    # Parameters:
    #   name: Name of the Myoband on the arm manager
    def get_orientation(self, name):
        id = self.__myo_names[name]
        myo = self.__myos[id]
        if(myo[0] is not None):
            return [myo[0].x, myo[0].y, myo[0].z, myo[0].w]
        return [0,0,0,1]

    # Gets the accelerometer data of the Myo band
    # Parameters:
    #   name: Name of the Myoband on the arm manager
    def get_accelerometer(self, name):
        id = self.__myo_names[name]
        myo = self.__myos[id]
        if(myo[1] is not None):
            return [myo[1].x, myo[1].y, myo[1].z]
        return [0,0,0]

    # Gets the pose of the Myo band
    # Poses are mapped to the integers in the if statements
    # 10 is equivalent to "unknown"
    # Parameters:
    #   name: Name of the Myoband on the arm manager
    def get_pose(self, name):
        id = self.__myo_names[name]
        myoband = self.__myos[id]
        if(myoband[2] is not None):
            if myoband[2] == myo.Pose.rest:
                return 0
            if myoband[2] == myo.Pose.fist:
                return 1
            if myoband[2] == myo.Pose.fingers_spread:
                return 2
            if myoband[2] == myo.Pose.wave_out:
                return 3
            if myoband[2] == myo.Pose.wave_in:
                return 4
        return 10

    # Returns a list of the states of two Myo bands for use with the IMUtJA2
    # class
    def state(self):
        states = [1, 0, 0, 0]
        forearm_id = self.__myo_names[self.__forearm_myo]
        shoulder_id = self.__myo_names[self.__shoulder_myo]

        states.append(self.__myos[forearm_id][0].w)
        states.append(self.__myos[forearm_id][0].x)
        states.append(self.__myos[forearm_id][0].y)
        states.append(self.__myos[forearm_id][0].z)

        states.append(self.__myos[shoulder_id][0].w)
        states.append(self.__myos[shoulder_id][0].x)
        states.append(self.__myos[shoulder_id][0].y)
        states.append(self.__myos[shoulder_id][0].z)

        return states
