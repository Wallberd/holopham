import sys
import datetime
import pickle

import time
import numpy as np
import multiprocessing as mp

from .. import ns_sleep

__HEADER_DELIMETER__ = b'\x00\xff\x00\xff\x00\xff'

def __split_file( f, delim = __HEADER_DELIMETER__, bufsize = 1024 ):
    prev = b''
    while True:
        s = f.read( bufsize )
        if not s: break
        split = s.split( delim )
        if len( split ) > 1:
            yield prev + split[ 0 ]
            prev = split[ -1 ]
            for x in split[1:-1]: yield x
        else: prev += s
        if prev: yield prev

def read_from_file( filename ):
    pfile = []
    for s in __split_file( open( filename, 'rb' ) ): pfile.append( s )
    return pickle.loads( pfile[-1] )

def write_to_file( stream, filename ):
    output = stream.flush()
    pkl_out = pickle.dumps( output, pickle.HIGHEST_PROTOCOL )
    with open( filename, 'w' ) as f:
        f.write( stream._create_header() )
        f.write( 'Size of Data in Bytes: ' + str( sys.getsizeof( pkl_out ) ) + '\n' )
        f.close()
    with open( filename, 'ab' ) as f:
        f.write( __HEADER_DELIMETER__ )
        f.write( pkl_out )
        f.close()

class DataStream:
    def __init__(self, hardware = None):
        self.__hardware = hardware
        self.__speriod = np.inf                             # initialize sampling rate

        # self.__manager = mp.Manager()
        self.__queue = mp.Queue()
        self.__state = { 'Time' : [] }# self.__manager.dict( { 'Time' : [] } )
        for dev in self.__hardware:
            self.__state.update( { dev.name : [] } )        # add device name as key to dictionary
            if dev.speriod < self.__speriod:            
                self.__speriod = dev.speriod                # update writer's sampling period if greater than device's

        self.__recording = mp.Event()
        self.__writing = mp.Event()
        self.__updated = mp.Event()
        self.__recorder = None

    def _create_header( self ):
        header = datetime.datetime.now().strftime( "File written on %d %b %Y at %I:%M:%S %p\n" )
        for dev in self.__hardware:
            header += type( dev ).__name__ + ' Interface named '
            header += dev.name + ',  ' 
            header += repr( dev.channelcount ) + ' Channels with Sampling Rate ' 
            header += repr( 1.0 / dev.speriod ) + ' Hz\n'
        return header

    def __record( self ):
        # setup local dictionary
        records = { 'Time' : [] }
        for dev in self.__hardware: records.update( { dev.name : [] } )

        # recording loop
        while self.__recording.is_set():                            # while recording
            if self.__writing.is_set():                             # if we are writing out
                self.__queue.put( records )
                self.__updated.set()                                # signal that we have updated
                for key in records.keys(): records[ key ] = []      # reset local dictionary
                while self.__writing.is_set(): ns_sleep( 100 )      # wait until no longer writing
                
            t = time.time() + self.__speriod                       # next read time
            records[ 'Time' ].append( time.time() )                # append new time to list
            for dev in self.__hardware:                            # for every device
                records[ dev.name ].append( dev.state.copy() )     # append new device state
                # print( dev.name, dev.state )
            while max( t - time.time(), 0 ): ns_sleep( 100 )       # wait until next read time

        # update before exiting, just in case
        if not self.__updated.is_set():
            self.__queue.put( records )
            self.__updated.set()                                    # signal that we have updated
            for key in records.keys(): records[ key ] = []          # reset local dictionary

    def start_record( self ):
        if not self.__recording.is_set():
            for dev in self.__hardware: dev.run()                       # start streaming for each device
            self.__recording.set()
            self.__recorder = mp.Process( target = self.__record )
            self.__recorder.start()

    def stop_record( self ):
        if self.__recording.is_set():
            self.__recording.clear()
            self.__recorder.join()
            for dev in self.__hardware: dev.stop()                      # stop each device stream

    def flush( self ):
        self.__writing.set()    # tell the recording process we want to flush the state
        self.__updated.wait()   # wait until shared state is updated
        self.__updated.clear()  # reset update flag
        
        self.__state = self.__queue.get().copy()                               # pull state from queue
        self.__state[ 'Time' ] = np.array( self.__state[ 'Time' ] )     # make time a numpy array
        for dev in self.__hardware:
            self.__state[ dev.name ] = np.vstack( self.__state[ dev.name ] )
        
        self.__writing.clear()
        return self.__state

    # TODO: Implement a peek function that works with multiprocessing
    # def peek( self ):
    #     output = self.__state.copy()       # copy to keep atomicity
    #     output[ 'Time' ] = np.array( output[ 'Time' ] )
    #     for dev in self.__hardware:
    #         key = dev.name
    #         output[ key ] = np.vstack( output[ key ] )
    #     return output
if __name__ == '__main__':
    from ..inputs import DebugDevice

    dbg = DebugDevice( srate = 100.0 )
    stream = DataStream( hardware = [ dbg ] )
    
    dbg.run( display = False )
    stream.start_record()
    print( 'Start Recording...' )
    time.sleep( 3 )
    stream.stop_record()
    print( 'Stop Recording...' )
    dbg.stop()

    tmpfile = 'tmp_datafile_' + datetime.date.today().strftime( "%d%b%Y" ) + '.pdata'
    write_to_file( stream, filename = tmpfile )

    data = read_from_file( tmpfile )
    print( repr( data[ 'Debug' ].shape ) )