# PLOT FUNCTIONS
import matplotlib
matplotlib.use('QT5Agg')

# TIME FUNCTIONS
import ctypes

LIBC = ctypes.CDLL('libc.so.6')

class Timespec( ctypes.Structure ):
    _fields_ = [ ('tv_sec', ctypes.c_long), ('tv_nsec', ctypes.c_long) ]
LIBC.nanosleep.argtypes = [ctypes.POINTER(Timespec), ctypes.POINTER(Timespec)]
nanosleep_req = Timespec()
nanosleep_rem = Timespec()

def ns_sleep( ns ):
    nanosleep_req.tv_sec = int( ns / 1e9 )
    nanosleep_req.tv_nsec = int( ns % 1e9 )
    LIBC.nanosleep( nanosleep_req, nanosleep_rem )

def us_sleep( us ):
    LIBC.usleep( int( us ) )
