import numpy as np

class MavFilter:
    def __init__(self, num_features = 1):
        assert ( num_features == 1 )
        self.__num_features = num_features

    def filter(self, raw):
        num_channels = raw.shape[1] if raw.ndim == 2 else raw.shape[0]
        feat = np.zeros( self.__num_features * num_channels )
        for chan in range( 0, num_channels ):
            idx = chan * self.__num_features
            feat[ idx ] = np.mean( np.abs( raw[ :, chan ] ) )    
        return feat

if __name__ == '__main__':
    data = np.random.rand( 1000, 8 )
    filt = MavFilter( num_features = 1 )
    features = filt.filter( data )
    print( features.shape )