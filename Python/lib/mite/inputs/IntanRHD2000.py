# NOTE: On Linux, run this command in terminal
#       sudo ln -s <libpath>/libudev.so.1 <libpath>/libudev.so.0
#       <libpath> found using -- sudo find / name "libudev.so*"
#       undo link with -- rm <libpath>/libudev.so.0

import os, sys
modpath = os.path.join( os.path.dirname( os.path.realpath( __file__ ) ), 'external', 'rhd2000' )
sys.path.append( modpath )
print(modpath)
import rhd2k

import time
import ctypes
import numpy as np

import multiprocessing as mp
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from scipy.interpolate import RectBivariateSpline
from scipy import signal

from .. import ns_sleep
from . import AbstractBaseInput
from ..filters import MavFilter

class IntanRHD2000(AbstractBaseInput):
    """ Python implementation of the Intan RHD2000 evaluation board"""
    BUFFER_SIZE = int( 60 )
    SAMPLERATE_MAPPING = { 1000 : rhd2k.Rhd2000EvalBoard.SampleRate1000Hz,
                           1250 : rhd2k.Rhd2000EvalBoard.SampleRate1250Hz, 
                           1500 : rhd2k.Rhd2000EvalBoard.SampleRate1500Hz,
                           2000 : rhd2k.Rhd2000EvalBoard.SampleRate2000Hz,
                           2500 : rhd2k.Rhd2000EvalBoard.SampleRate2500Hz,
                           3000 : rhd2k.Rhd2000EvalBoard.SampleRate3000Hz,
                           3333 : rhd2k.Rhd2000EvalBoard.SampleRate3333Hz,
                           4000 : rhd2k.Rhd2000EvalBoard.SampleRate4000Hz,
                           5000 : rhd2k.Rhd2000EvalBoard.SampleRate5000Hz,
                           6250 : rhd2k.Rhd2000EvalBoard.SampleRate6250Hz,
                           8000 : rhd2k.Rhd2000EvalBoard.SampleRate8000Hz,
                           10000 : rhd2k.Rhd2000EvalBoard.SampleRate10000Hz,
                           12500 : rhd2k.Rhd2000EvalBoard.SampleRate12500Hz,
                           15000 : rhd2k.Rhd2000EvalBoard.SampleRate15000Hz,
                           20000 : rhd2k.Rhd2000EvalBoard.SampleRate20000Hz,
                           25000 : rhd2k.Rhd2000EvalBoard.SampleRate25000Hz,
                           30000 : rhd2k.Rhd2000EvalBoard.SampleRate30000Hz }
    DATAPORT_MAPPING = { 'A1' : rhd2k.Rhd2000EvalBoard.PortA1,
                         'B1' : rhd2k.Rhd2000EvalBoard.PortB1,
                         'C1' : rhd2k.Rhd2000EvalBoard.PortC1,
                         'D1' : rhd2k.Rhd2000EvalBoard.PortD1, 
                         'A2' : rhd2k.Rhd2000EvalBoard.PortA2,
                         'B2' : rhd2k.Rhd2000EvalBoard.PortB2,
                         'C2' : rhd2k.Rhd2000EvalBoard.PortC2,
                         'D2' : rhd2k.Rhd2000EvalBoard.PortD2 }
    ELECTRODE_MAPPING = [ 24, 25, 26, 27, 28, 29, 30, 31, 23, 22, \
                          21, 20, 19, 18, 17, 16,  8,  9, 10, 11, \
                          12, 13, 14, 15,  7,  6,  5,  4,  3,  2, \
                           1,  0 ]

    @staticmethod
    def microvolts( data ): return 0.195 * ( data - 32768 )
    
    def __init__(self, name = 'RHD2000', ports = ['A1','B1','C1','D1'], channels_per_port = 32, \
                 bandwidth = [1.0, 500.0], dc_offset = 10.0, notch = 60.0, srate = 1250.0 ):
        """ Constructor """
        # Four ports: A1, B1, C1, D1
        # can use 8 to 64 electrodes per port (currently using 32)
        # sampling rate: 1250.0 Hz
        # buffer size: 60 samples
        # notch filter: 60 Hz

        # hardware variables
        assert( set( ports ).issubset( ['A1','A2','B1','B2','C1','C2','D1','D2'] ) )
        self.__ports = tuple( ports )

        if isinstance( channels_per_port, int ):
            channels = ( ( channels_per_port, ) * len( self.__ports ) )
        else:
            assert( len( channels_per_port ) == len( self.__ports ) )
            channels = channels_per_port
        self.__samplingrate = int( srate )

        self.__board = None
        self.__rhdqueue = None
        self.__amp_channels = channels

        # device variables
        self.__name = name
        self.__channelcount = int( np.asarray( channels ).sum() )
        self.__speriod = IntanRHD2000.BUFFER_SIZE / srate
        self.__state = mp.Array( ctypes.c_double, self.__channelcount * IntanRHD2000.BUFFER_SIZE )

        # frequency variables
        self.__bandwidth = tuple( bandwidth )
        self.__dc_offset = dc_offset

        if notch is not None: 
            assert( notch == 50.0 or notch == 60.0 )
            center = notch / ( srate / 2 )
            qfactor = 30
            b, a = signal.iirnotch( center, qfactor )
            zi = np.matlib.repmat( signal.lfilter_zi( b, a ), self.__channelcount, 1 ).T
            self.__notch = { 'a' : a, 'b' : b, 'z' : zi }

            # w, h = freqz( self.__notch[0], self.__notch[1] )
            # freq = w * srate / ( 2 * np.pi )
            # fig, ax = plt.subplots( 2, 1, figsize = (8,6) )
            # ax[0].plot( freq, 20 * np.log10( abs( h ) ), color = 'blue' )
            # ax[1].plot( freq, np.unwrap( np.angle( h ) ) * 180 / np.pi, color = 'green' )
            # plt.show()
            # exit()
        else: self.__notch = None

        # synchronization variables
        self.__exit_event = mp.Event()
        self.__conn_event = mp.Event()

        # streaming variables
        self.__stream_event = mp.Event()
        self.__print_event = mp.Event()
        self.__streamer = None

        # viewing variables
        self.__view_event = mp.Event()
        self.__queue = mp.Queue()
        self.__viewer = None

        # self.__temp = mp.Event()

        self.__streamer = mp.Process( target = self.__connect )
        self.__streamer.daemon = True
        self.__streamer.start()
        self.__conn_event.wait()

    def __del__(self):
        try:
            if self.__streamer.is_alive(): 
                self.__exit_event.set()
                self.stop()
        except AttributeError: pass
        try:
            if self.__viewer.is_alive(): self.hide()
        except AttributeError: pass

    def __connect(self):
        """ Connect and initialize the RHD2000 evaluation board """
        try:
            curdir = os.getcwd()    # NOTE: Temporary fix for not finding okFrontPanel dynamic lib
            os.chdir( os.path.join( os.path.dirname( os.path.realpath( __file__ ) ), 'external', 'rhd2000'))#, 'lib' ) ) # NOTE: Temporary fix for not finding okFrontPanel dynamic lib
            self.__board = rhd2k.Rhd2000EvalBoard()        # create board
            self.__board.open()                            # open board
            os.chdir( curdir )      # NOTE: Temporary fix for not finding okFrontPanel dynamic lib
            self.__board.uploadFpgaBitfile( os.path.join( os.path.dirname( os.path.realpath( __file__ ) ), 'external', 'rhd2000', 'main.bit' ) )     # upload main.bit
            self.__board.initialize()                      # initialize

            # Steps for initializing RHD2000:
            # 1) Enable data streams and assign data sources to each stream.
            # 2) Set sampling rate and delay (based on cable length).
            # 3) Initialize data queue
            # 4) Initialize and calibrate on-board ADC via command list
            #    i) Initialize command list
            #   ii) Initialize chip registers with upper/lower bandwidth limits
            #  iii) Upload command lists w/ and w/o ADC calibration to different command banks
            #   iv) Select command bank w/ ADC calibration and run board until command sequence completes (60 samples)
            #    v) Remove collected data samples from queue
            #   vi) Select command bank w/o ADC calibration
            # 5) Change board to continuous run mode and start data acquisition
            
            # Enable or disable each of the eight available USB data streams (0-7).
            self.__board.enableDataStream( 0, True )
            self.__board.enableDataStream( 1, False )
            self.__board.enableDataStream( 2, True )
            self.__board.enableDataStream( 3, False )
            self.__board.enableDataStream( 4, True )
            self.__board.enableDataStream( 5, False )
            self.__board.enableDataStream( 6, True )
            self.__board.enableDataStream( 7, False )
            
            # Assign particular data sources to the enabled USB data streams.
            self.__board.setDataSource( 0, rhd2k.Rhd2000EvalBoard.PortA1)
            #self.__board.setDataSource( 0, rhd2k.Rhd2000EvalBoard.PortA2)
            self.__board.setDataSource( 2, rhd2k.Rhd2000EvalBoard.PortB1)
            #self.__board.setDataSource( 0, rhd2k.Rhd2000EvalBoard.PortB2)
            self.__board.setDataSource( 4, rhd2k.Rhd2000EvalBoard.PortC1)
            #self.__board.setDataSource( 0, rhd2k.Rhd2000EvalBoard.PortC2)
            self.__board.setDataSource( 6, rhd2k.Rhd2000EvalBoard.PortD1)
            #self.__board.setDataSource( 0, rhd2k.Rhd2000EvalBoard.PortD2)
            
            # Set sampling rate
            self.__board.setSampleRate( IntanRHD2000.SAMPLERATE_MAPPING[ self.__samplingrate ] )
            
            # Set the delay for sampling each SPI port based on the length of the cable  
            # between  the  FPGA  and  the  RHD2000  chip  (in  meters). Cable  delay  
            # should  be  updated  after  any  changes  are  made to the sampling rate.
            self.__board.setCableLengthMeters( rhd2k.Rhd2000EvalBoard.PortA, 1.0 )
            self.__board.setCableLengthMeters( rhd2k.Rhd2000EvalBoard.PortB, 1.0 )
            self.__board.setCableLengthMeters( rhd2k.Rhd2000EvalBoard.PortC, 1.0 )
            self.__board.setCableLengthMeters( rhd2k.Rhd2000EvalBoard.PortD, 1.0 )
            
            # Initialize data queue for streaming data
            self.__rhdqueue = rhd2k.dataqueue.DataQueue()

            # set frequency parameters
            chip_registers = rhd2k.Rhd2000Registers( self.__board.getSampleRate() )
            dsp_cutoff_freq = chip_registers.setDspCutoffFreq( self.__dc_offset )       # remove DC offset (10x LPF for best linearity)
            chip_registers.setLowerBandwidth( self.__bandwidth[0] )                     # amplifier lower bandwidth
            chip_registers.setUpperBandwidth( self.__bandwidth[1] )                     # amplifier upper bandwidth

            self.__commandList = rhd2k.VectorInt() #initialize command list
            
            # Upload command list version with ADC calibration to AuxCmd3 RAM Bank 0.
            self.__commandSequenceLength = chip_registers.createCommandListRegisterConfig(self.__commandList, True)
            self.__board.uploadCommandList(self.__commandList, rhd2k.Rhd2000EvalBoard.AuxCmd3, 0)
            self.__board.selectAuxCommandLength(rhd2k.Rhd2000EvalBoard.AuxCmd3, 0, self.__commandSequenceLength - 1)
            
            # Upload command list version with no ADC calibration to AuxCmd3 RAM Bank 1.
            chip_registers.setFastSettle(False)
            self.__commandSequenceLength = chip_registers.createCommandListRegisterConfig(self.__commandList, False)
            self.__board.uploadCommandList(self.__commandList, rhd2k.Rhd2000EvalBoard.AuxCmd3, 1)
            self.__board.selectAuxCommandLength(rhd2k.Rhd2000EvalBoard.AuxCmd3, 0, self.__commandSequenceLength - 1)

            # Select command bank with ADC calibration to initialize SPI ports
            self.__board.selectAuxCommandBank(rhd2k.Rhd2000EvalBoard.PortA, rhd2k.Rhd2000EvalBoard.AuxCmd3, 0)
            self.__board.selectAuxCommandBank(rhd2k.Rhd2000EvalBoard.PortB, rhd2k.Rhd2000EvalBoard.AuxCmd3, 0)
            self.__board.selectAuxCommandBank(rhd2k.Rhd2000EvalBoard.PortC, rhd2k.Rhd2000EvalBoard.AuxCmd3, 0)
            self.__board.selectAuxCommandBank(rhd2k.Rhd2000EvalBoard.PortD, rhd2k.Rhd2000EvalBoard.AuxCmd3, 0)

            # Since our longest command sequence is 60 commands, we run the SPI
            # interface for 60 samples.
            self.__board.setMaxTimeStep(60)
            self.__board.setContinuousRunMode(False)

            # Start SPI interface.
            self.__board.run()

            # Wait for the 60-sample run to complete.
            while self.__board.isRunning():
                pass
            
            # Remove collected samples from FIFO queue
            self.__board.readDataBlocks( 1, self.__rhdqueue)
            self.__rhdqueue.pop()
            self.__board.setMaxTimeStep( 0 ) # when we say stop, we mean it


            # Now that the ADC calibration is complete, switch to the command bank with no ADC calibration
            self.__board.selectAuxCommandBank(rhd2k.Rhd2000EvalBoard.PortA, rhd2k.Rhd2000EvalBoard.AuxCmd3, 1)
            self.__board.selectAuxCommandBank(rhd2k.Rhd2000EvalBoard.PortB, rhd2k.Rhd2000EvalBoard.AuxCmd3, 1)
            self.__board.selectAuxCommandBank(rhd2k.Rhd2000EvalBoard.PortC, rhd2k.Rhd2000EvalBoard.AuxCmd3, 1)
            self.__board.selectAuxCommandBank(rhd2k.Rhd2000EvalBoard.PortD, rhd2k.Rhd2000EvalBoard.AuxCmd3, 1)
            
            leds = [ 0 ] * 8
            for i in range( 0, len( self.__ports ) ): leds[ i ] = 1
            self.__board.setLedDisplay( leds )        # turn LED on
            self.__conn_event.set()

            # run board
            # self.__board.setContinuousRunMode( True )
            # self.__board.run()
            
            while not self.__exit_event.is_set(): # and self.__board.isRunning():    # while we are not exiting
               if self.__stream_event.is_set(): 
                   
                   # start streaming
                   self.__board.setContinuousRunMode( True )
                   self.__board.run()
                   
                   self.__stream()     # stream when asked
                   # stop board
                #    self.__setMaxTimeStep( 0 )
                   self.__board.setContinuousRunMode( False )
                   self.__board.flush()
                   
        finally:
            self.__board.setLedDisplay( [ 0, 0, 0, 0, 0, 0, 0 ,0 ] )
            self.__conn_event.set()

    def __read(self):
        """ Reads a single sample from the debug device """
        newDataReady = self.__board.readDataBlocks( 1, self.__rhdqueue)
        
        if newDataReady:
            # Load and scale RHD2000 amplifier waveforms
            # (sampled at amplifier sampling rate)
            front_ = self.__rhdqueue.front()
            self.amplifierPreFilter = np.zeros( (len(self.__ports)*32, IntanRHD2000.BUFFER_SIZE) )
            for stream in range( len(self.__ports) ):
                stream_ = front_.amplifierData[stream]
                for channel in range( 32 ):
                    channel_ = stream_[channel]
                    idx = stream * 32 + channel
                    prefilter_ = self.amplifierPreFilter[idx]
                    for t in range( IntanRHD2000.BUFFER_SIZE ):                            
                        prefilter_[t] = 0.195 * (channel_[t] - 32768) # Amplifier waveform units = microvolts
            self.__rhdqueue.pop()
            data = self.amplifierPreFilter.T

            # Filter power line noise if notch filter is enabled
            if self.__notch is not None:
                for i in range( self.__channelcount ):
                    data[:,i], self.__notch['z'][:,i] = signal.lfilter( self.__notch['b'], self.__notch['a'], \
                                                        data[:,i], zi = self.__notch['z'][:,i] )
            
            # Remap data to match physcial layout of electrodes
            data_remap = np.zeros((IntanRHD2000.BUFFER_SIZE,16,8))
            for t in range(data.shape[0]):
                idx1=0
                for i in range( 0, len( self.__ports ) ):
                    idx2 = idx1 + self.__amp_channels[i]
                    data[t,idx1:idx2] = ( data[t,idx1:idx2] )[IntanRHD2000.ELECTRODE_MAPPING]
                    idx1 = idx2                  
                data_remap[t] = np.reshape( data[t], ( 16, 8 ) )
            data_remap = np.transpose( data_remap, ( 0,2,1 ) )
            
            ## Convert from monopolar to differential electrode configuration
            # data_diff = np.zeros(data_remap.shape)
            # for t in range(data_remap.shape[0]):
            #    data_diff[t] = signal.convolve2d(data_remap[t], np.reshape([0.5,-0.5], (1,2)), boundary='wrap', mode='same')

            # Calculate the mean absolute value (MAV) of the 60 samples
            data_mav = np.mean( np.abs( data_remap ), axis = 0 )

            self.__state[:] = data.flatten()

            if self.__print_event.is_set(): print( self.__name, ':', data )
            #if self.__view_event.is_set(): self.__queue.put( data[-1,:] )
            if self.__view_event.is_set(): self.__queue.put( data_mav )

    def __stream(self):
        """ Streams data from the debug device at the specified sampling rate """
        t = time.time()
        # ttmp = time.time()
        while self.__stream_event.is_set():
            # if ( time.time() - ttmp ) > 25: self.__temp.set()
            t = t + self.__speriod
            self.__read()
            while max( t - time.time(), 0 ): ns_sleep( 1e2 )

    def __plot(self, imshape):
        """ Update the Intan plots """
        # initialize the figure
        gui, ax = plt.subplots()
        gui.canvas.set_window_title( self.__name )
        plt.subplots_adjust( bottom = 0.15 )

        vmin = IntanRHD2000.microvolts( 0 )
        vmax = IntanRHD2000.microvolts( 2 ** 16 - 1 )
        img = ax.imshow( np.zeros( imshape ), cmap = 'jet', vmin = vmin, vmax = vmax )
        plt.axis( 'off' )

        axfilter = plt.axes( [0.25, 0.05, 0.65, 0.03], facecolor = 'lightgoldenrodyellow' )
        sfilt = Slider( axfilter, 'Multiplier', 1.0, 50.0, valinit = 1.0 )

        # stream
        plt.show( block = False )
        x =  np.linspace( 0, imshape[0], imshape[0] )
        y =  np.linspace( 0, imshape[1], imshape[1] )
        while self.__view_event.is_set():
            try:
                data = None
                while self.__queue.qsize() > 0: data = self.__queue.get()
                if data is not None:
                    # remap data for appropriate display
                    # idx1 = 0
                    # for i in range( 0, len( self.__ports ) ):
                    #     idx2 = idx1 + self.__amp_channels[i]
                    #     data[idx1:idx2] = ( data[idx1:idx2] )[IntanRHD2000.ELECTRODE_MAPPING]
                    #     idx1 = idx2
                    
                    # data = np.reshape( data, ( imshape[1], imshape[0] ) ).T # complicated reshape to match our HDEMG array 
                    
                    
                    
                    # data = data / ( 0.0975 / 2 )                                    # scale between 0 and 1
                    if sfilt.val != 1.0:
                        interp = RectBivariateSpline( x, y, data )
                        xnew = np.linspace( 0, imshape[0], np.round( sfilt.val * imshape[0] ) )
                        ynew = np.linspace( 0, imshape[1], np.round( sfilt.val * imshape[1] ) )
                        data = interp( xnew, ynew )
                    img.set_data( data )
                plt.pause( 0.05 )
            except: self.__view_event.clear()
        plt.close( gui )

    @property
    def name(self): return self.__name

    @property
    def state(self): return np.frombuffer( self.__state.get_obj() ).copy().reshape( IntanRHD2000.BUFFER_SIZE, self.__channelcount )

    @property
    def speriod(self): return self.__speriod

    @property
    def channelcount(self): return self.__channelcount

    def run(self, display = False):
        """ Starts the acquisition process of the debug device """
        if not self.__stream_event.is_set():
            self.__stream_event.set()
            if display: self.__print_event.set()
            else: self.__print_event.clear()

    def stop(self):
        '''Stops the acquisition process of the debug device'''
        if self.__stream_event.is_set():
            self.__stream_event.clear()

    def view(self, imshape = (8, 16) ):
        """ Launches the GUI viewer of the debug device data """
        if not self.__view_event.is_set():
            self.__view_event.set()
            self.__viewer = mp.Process( target = self.__plot, args = ( imshape, ) )
            self.__viewer.start()
            
    def hide(self):
        '''Closes the GUI viewer of the Intan data'''
        if self.__view_event.is_set():
            self.__view_event.clear()
            self.__viewer.join()

if __name__ == '__main__':
    import sys
    import inspect
    import argparse

    # helper function for booleans
    def str2bool( v ):
        if v.lower() in [ 'yes', 'true', 't', 'y', '1' ]: return True
        elif v.lower() in [ 'no', 'false', 'n', 'f', '0' ]: return False
        else: raise argparse.ArgumentTypeError( 'Boolean value expected!' )

    # parse commandline entries
    class_init = inspect.getargspec( IntanRHD2000.__init__ )
    arglist = class_init.args[1:]   # first item is always self
    defaults = class_init.defaults
    parser = argparse.ArgumentParser()
    for arg in range( 0, len( arglist ) ):
        try: tgt_type = type( defaults[ arg ][ 0 ] )
        except: tgt_type = type( defaults[ arg ] )
        if tgt_type is bool:
            parser.add_argument( '--' + arglist[ arg ], 
                             type = str2bool, nargs = '?',
                             action = 'store', dest = arglist[ arg ],
                             default = defaults[ arg ] )
        else:
            parser.add_argument( '--' + arglist[ arg ], 
                                type = tgt_type, nargs = '+',
                                action = 'store', dest = arglist[ arg ],
                                default = defaults[ arg ] )
    args = parser.parse_args()
    for arg in range( 0, len( arglist ) ):
        attr = getattr( args, arglist[ arg ] )
        if isinstance( attr, list ) and not isinstance( defaults[ arg ], list ):
            setattr( args, arglist[ arg ], attr[ 0 ]  )

    intan = IntanRHD2000( name = args.name, ports = args.ports, channels_per_port = args.channels_per_port, \
                          bandwidth = args.bandwidth, srate = args.srate )
    intan.run( display = False )
    intan.view()
