import time
import ctypes
import numpy as np

import multiprocessing as mp
import matplotlib.pyplot as plt

from .. import ns_sleep
from . import AbstractBaseInput

class DebugDevice(AbstractBaseInput):
    """ Python implementation of a fake hardware device for debug purposes """

    def __init__(self, name = 'Debug', num_channels = 8, srate = 100.0):
        """ Constructor """
        # device variables
        self.__name = name
        self.__channelcount = num_channels
        self.__speriod = 1.0 / srate
        self.__state = mp.Array( ctypes.c_double, self.__channelcount )

        # streaming variables
        self.__stream_event = mp.Event()
        self.__print_event = mp.Event()
        self.__streamer = None

        # viewing variables -- not really used right now
        self.__view_event = mp.Event()
        self.__queue = mp.Queue()
        self.__viewer = None

    def __del__(self):
        try:
            if self.__streamer.is_alive(): self.stop()
        except AttributeError: pass
        try:
            if self.__viewer.is_alive(): self.hide()
        except AttributeError: pass

    def __read(self):
        """ Reads a single sample from the debug device """
        self.__state[:] = np.random.rand( self.__channelcount )
        if self.__print_event.is_set(): print( self.__name, ':', np.frombuffer( self.__state.get_obj() ) )
        
    def __stream(self):
        """ Streams data from the debug device at the specified sampling rate """
        t = time.time()
        while self.__stream_event.is_set():
            t = t + self.__speriod
            self.__read()
            while max( t - time.time(), 0 ): ns_sleep( 1e2 )

    def __plot(self):
        pass

    @property
    def name(self): return self.__name

    @property
    def state(self): return np.frombuffer( self.__state.get_obj() ).copy()

    @property
    def speriod(self): return self.__speriod

    @property
    def channelcount(self): return self.__channelcount

    def run(self, display = False):
        """ Starts the acquisition process of the debug device """
        if not self.__stream_event.is_set():
            self.__stream_event.set()
            if display: self.__print_event.set()
            else: self.__print_event.clear()
            
            self.__streamer = mp.Process( target = self.__stream )
            self.__streamer.start()

    def stop(self):
        '''Stops the acquisition process of the debug device'''
        if self.__stream_event.is_set():
            self.__stream_event.clear()
            self.__streamer.join()

    def view(self):
        """ Launches the GUI viewer of the debug device data """
        pass
        if not self.__view_event.is_set():
            self.__view_event.set()
            self.__viewer = mp.Process( target = self.__plot )
            self.__viewer.start()
            
    def hide(self):
        '''Closes the GUI viewer of the debug device data'''
        if self.__view_event.is_set():
            self.__view_event.clear()
            self.__viewer.join()

if __name__ == '__main__':
    import sys
    import inspect
    import argparse

    # helper function for booleans
    def str2bool( v ):
        if v.lower() in [ 'yes', 'true', 't', 'y', '1' ]: return True
        elif v.lower() in [ 'no', 'false', 'n', 'f', '0' ]: return False
        else: raise argparse.ArgumentTypeError( 'Boolean value expected!' )

    # parse commandline entries
    class_init = inspect.getargspec( DebugDevice.__init__ )
    arglist = class_init.args[1:]   # first item is always self
    defaults = class_init.defaults
    parser = argparse.ArgumentParser()
    for arg in range( 0, len( arglist ) ):
        try: tgt_type = type( defaults[ arg ][ 0 ] )
        except: tgt_type = type( defaults[ arg ] )
        if tgt_type is bool:
            parser.add_argument( '--' + arglist[ arg ], 
                             type = str2bool, nargs = '?',
                             action = 'store', dest = arglist[ arg ],
                             default = defaults[ arg ] )
        else:
            parser.add_argument( '--' + arglist[ arg ], 
                                type = tgt_type, nargs = '+',
                                action = 'store', dest = arglist[ arg ],
                                default = defaults[ arg ] )
    args = parser.parse_args()
    for arg in range( 0, len( arglist ) ):
        attr = getattr( args, arglist[ arg ] )
        if isinstance( attr, list ) and not isinstance( defaults[ arg ], list ):
            setattr( args, arglist[ arg ], attr[ 0 ]  )

    dbg = DebugDevice( num_channels = args.num_channels, srate = args.srate )
    dbg.run( display = True )
    dbg.view()
