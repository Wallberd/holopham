# NOTE: This interface requires that the Myo Armband be previously set up using MyoConnect on
#       a Windows or Mac PC.'

import re
import sys

import time
import struct
import ctypes

import serial
from serial.tools.list_ports import comports

import numpy as np
import multiprocessing as mp
import matplotlib.pyplot as plt

from .. import ns_sleep
from ..utils import Quaternion as quat
from . import AbstractBaseInput

class MyoArmband(AbstractBaseInput):
    """ Python interface for a Myo Armband from Thalmic Labs  """
    CONNECTION_TIMEOUT = 10.0

    MYOHW_ORIENTATION_SCALE = 16384.0
    MYOHW_ACCELEROMETER_SCALE = 2048.0
    MYOHW_GYROSCOPE_SCALE = 16.0

    ''' Static methods for data manipulation '''
    @staticmethod
    def pack( fmt, *args ):
        return struct.pack( '<' + fmt, *args )

    @staticmethod 
    def unpack( fmt, *args ):
        return struct.unpack( '<' + fmt, *args )

    @staticmethod
    def multichr( ords ):
        if sys.version_info[0] >= 3: return bytes( ords )
        else: return ''.join( map( chr, ords ) )

    @staticmethod
    def multiord( b ):
        if sys.version_info[0] >= 3: return list( b )
        else: return map( ord, b )

    ''' Private class to wrap Myo Armband bluetooth packet structure '''
    class Packet( object ):
        def __init__( self, ords ):
            self.typ = ords[0]
            self.cls = ords[2]
            self.cmd = ords[3]
            self.payload = MyoArmband.multichr( ords[4:] )

        def __repr__(self):
            return 'Packet(%02X, %02X, %02X, [%s])' % \
                (self.typ, self.cls, self.cmd,
                ' '.join('%02X' % b for b in MyoArmband.multiord(self.payload)))
    
    ''' Private class that implements the non-Myo-specific details of the Bluetooth protocol '''
    class MyoBT( object ):
        def __init__(self, tty ):
            self.ser = serial.Serial( port = tty, baudrate = 9600, dsrdtr = 1 )
            self.buf = []
            self.handlers = []

        # internal data-handling methods
        def recv_packet( self, timeout = None ):
            t0 = time.time()
            self.ser.timeout = None
            while timeout is None or time.time() < t0 + timeout:
                if timeout is not None: self.ser.timeout = max( 0, t0 + timeout - time.time() )
                c = self.ser.read()
                if not c: return None

                ret = self.proc_byte( ord( c ) )
                if ret:
                    if ret.typ == 0x80: self.handle_event( ret )
                    return ret

        def proc_byte( self, c ):
            if not self.buf:
                if c in [ 0x00, 0x80, 0x08, 0x88 ]: # [ BLE response packet, BLE event packet, Wifi response packet, Wifi event packet ]
                    self.buf.append( c )
                return None
            elif len( self.buf ) == 1:
                self.buf.append( c )
                self.packet_len = 4 + ( self.buf[ 0 ] & 0x07 ) + self.buf[ 1 ]
                return None
            else: self.buf.append( c )

            if self.packet_len and len( self.buf ) == self.packet_len:
                p = MyoArmband.Packet( self.buf )
                self.buf = []
                return p
            return None

        def handle_event( self, p ):
            for h in self.handlers: h( p )

        def add_handler( self, h ): self.handlers.append( h )

        def remove_handler( self, h ):
            try:
                self.handlers.remove( h )
            except ValueError: pass

        def wait_event( self, cls, cmd ):
            res = [ None ]
            def h( p ):
                if ( p.cls, p.cmd ) == ( cls, cmd ): res[0] = p
            self.add_handler( h )
            while res[0] is None: self.recv_packet()
            self.remove_handler( h )
            return res[0]

        # specific BLE commands
        def connect( self, addr ):
            return self.send_command( 6, 3, MyoArmband.pack( '6sBHHHH', MyoArmband.multichr( addr ), 0, 6, 6, 64, 0 ) )

        def get_connections( self ):
            return self.send_command( 0, 6 )

        def discover( self ):
            return self.send_command( 6, 2, b'\x01' )

        def end_scan( self ):
            return self.send_command( 6, 4 )

        def disconnect( self, h ):
            return self.send_command( 3, 0, MyoArmband.pack( 'B', h ) )

        def read_attr( self, con, attr ):
            self.send_command( 4, 4, MyoArmband.pack( 'BH', con, attr ) )
            return self.wait_event( 4, 5 )

        def write_attr( self, con, attr, val ):
            self.send_command( 4, 5, MyoArmband.pack( 'BHB', con, attr, len( val ) ) + val )
            return self.wait_event( 4, 1 )

        def send_command( self, cls, cmd, payload = b'', wait_resp = True ):
            s = MyoArmband.pack( '4B', 0, len( payload ), cls, cmd ) + payload
            self.ser.write( s )

            while True:
                p = self.recv_packet()
                if p.typ == 0: return p
                self.handle_event( p )
    
    ''' Myo-specific communication protocol '''

    def __init__(self, name = 'MyoArmband', com = None, mac = 'e0:f2:99:e7:60:40', srate = 200.0, raw = True):
        """ Constructor """
        self.__name = name
        self.__mac = bytes.fromhex( ''.join( mac.split(':') ) )
        self.__raw = raw

        # set device variables
        self.__channelcount = 12
        self.__speriod = 1.0 / srate
        
        # bluetooth connection variables
        self.__bt = None
        self.__conn = None

        # raw EMG callback variables
        self.__emg_time = mp.Value( ctypes.c_double, 0.0 )
        self.__emg_value = mp.Array( ctypes.c_byte, 16 )
        
        # synchronization events
        self.__exit_event = mp.Event()
        self.__conn_event = mp.Event()

        # streaming variables
        self.__state = mp.Array( ctypes.c_double, self.__channelcount )
        self.__state[8:] = np.array( [ 1.0, 0.0, 0.0, 0.0 ] ) # initialize quaternion
        self.__stream_event = mp.Event()
        self.__print_event = mp.Event()
        self.__streamer = None

        # viewing variables
        self.__view_event = mp.Event()
        self.__queue = mp.Queue()
        self.__viewer = None

        self.__streamer = mp.Process( target = self.__connect, args = ( com, ) )
        self.__streamer.start()
        
        self.__conn_event.wait()

    def __del__(self):
        """ Destructor """
        try:
            if self.__streamer.is_alive:
                self.__exit_event.set()
                self.__streamer.stop()
                self.__streamer.join()
        except AttributeError: pass # never got to make the I/O thread
        try:
            if self.__viewer.is_alive: self.hide()
        except AttributeError: pass # no viewer exists currently

    def __detect_tty(self):
        for p in comports():
            if re.search( r'PID=2458:0*1', p[2] ): return p[0]
        return None

    def __write_attr( self, attr, val ):
        if self.__conn is not None:
            self.__bt.write_attr( self.__conn, attr, val )

    def __read_attr( self, attr ):
        if self.__conn is not None:
            return self.__bt.read_attr( self.__conn, attr )
        return None

    def __connect(self, com):
        # standard initialization stuff
        if com is None: com = self.__detect_tty()
        if com is None: raise RuntimeError( 'Cannot discover BlueGiga dongle!' )
            
        self.__bt = MyoArmband.MyoBT( com )

        # connection
        try:
            # stop everything from before
            self.__bt.end_scan()
            for i in range( 0, 3 ): self.__bt.disconnect( i )
            
            # start scanning
            addr = None
            self.__bt.discover()
            t0 = time.time()
            while ( time.time() - t0 < MyoArmband.CONNECTION_TIMEOUT ):
                p = self.__bt.recv_packet()
                if p.payload.endswith( b'\x06\x42\x48\x12\x4a\x7f\x2c\x48\x47\xb9\xde\x04\xa9\x01\x00\x06\xd5' ):
                    if p.payload[2:8] == self.__mac[::-1]:
                        addr = list( MyoArmband.multiord( p.payload[2:8] ) )
                        break
            if addr is None: raise ValueError( 'Could not find Myo Armband with MAC address', self.__mac )
            self.__bt.end_scan()

            # connect and wait for status event
            conn_pkt = self.__bt.connect( addr )
            self.__conn = MyoArmband.multiord( conn_pkt.payload )[-1]
            self.__bt.wait_event( 3, 0 )

            # get firmware version
            fw = self.__read_attr( 0x17 )
            _, _, _, _, v0, _, _, _ = MyoArmband.unpack( 'BHBBHHHH', fw.payload )
            if v0 == 0: # old firmware
                self.__write_attr( 0x19, b'\x01\x02\x00\x00' )
                # subscribe to EMG data notifications
                self.__write_attr( 0x2f, b'\x01\x00' )
                self.__write_attr( 0x2c, b'\x01\x00' )
                self.__write_attr( 0x32, b'\x01\x00' )
                self.__write_attr( 0x35, b'\x01\x00' )

                
                self.__write_attr( 0x28, b'\x01\x00' )  # enable EMG data
                self.__write_attr( 0x1d, b'\x01\x00' )  # enable IMU data

                # set up streaming parameters
                C = 1000
                emg_hz = 50
                emg_smooth = 100
                imu_hz = 50
                self.__write_attr( 0x19, MyoArmband.pack( 'BBBBHBBBBB', 2, 9, 2, 1, C, emg_smooth, C // emg_hz, imu_hz, 0, 0 ) )
            else:
                # name = self.__read_attr( 0x03 )         # get device name, can use this later
                self.__write_attr( 0x1d, b'\x01\x00' )  # enable imu
                # self.__write_attr( 0x24, b'\x02\x00' )  # enable on/off arm notifications

                # enable emg streaming
                self.__write_attr( 0x2c, b'\x01\x00' )  # subscribe to EmgData0Characteristic
                self.__write_attr( 0x2f, b'\x01\x00' )  # subscribe to EmgData1Characteristic
                self.__write_attr( 0x32, b'\x01\x00' )  # subscribe to EmgData2Characteristic
                self.__write_attr( 0x35, b'\x01\x00' )  # subscribe to EmgData3Characteristic

                if self.__raw: 
                    self.__write_attr( 0x19, b'\x01\x03\x02\x01\x00' )
                else:                      # hidden functionality, no guarantees moving forward
                    self.__write_attr( 0x28, b'\x01\x00' )
                    self.__write_attr( 0x19, b'\x01\x03\x01\x01\x00' )

            # self.__write_attr( 0x12, b'\x01\x10' )       # enable battery notifications
            self.__write_attr( 0x19, MyoArmband.pack( '3B', 9, 1, 1 ) ) # turn off sleep

            def handle_data( p ):
                if ( p.cls, p.cmd ) != ( 4, 5 ): return
                _, attr, _ = MyoArmband.unpack( 'BHB', p.payload[:4] )
                pay = p.payload[5:]
                if attr == 0x27:                                                    # filtered EMG
                    self.__state[:8] = MyoArmband.unpack( '8HB', pay )[:8]
                elif attr == 0x2b or attr == 0x2e or attr == 0x31 or attr == 0x34:  # raw EMG
                    self.__emg_value[:] = struct.unpack( '<16b', pay )
                    self.__emg_time.value = time.time()
                elif attr == 0x1c:                                                  # IMU
                    self.__state[8:] = np.array( MyoArmband.unpack( '10h', pay )[:4] ) / MyoArmband.MYOHW_ORIENTATION_SCALE
                else: pass                                                          # unsupported

            self.__bt.add_handler( handle_data )
            self.__conn_event.set()
            while not self.__exit_event.is_set():                   # whle we are not exiting
               if self.__stream_event.is_set(): self.__stream()     # stream when asked
        finally:
            self.__conn_event.set()
            self.__disconnect()

    def __disconnect(self):
        if self.__conn is not None:
            self.__bt.disconnect( self.__conn )

    def __read(self):
        """ Reads a single sample from the MyoArmband """
        self.__bt.recv_packet( self.__speriod )
        if self.__raw:
            idx = 8 * ( ( time.time() - self.__emg_time.value ) > 5e-3 )
            self.__state[:8] = self.__emg_value[idx:idx+8]

        if self.__print_event.is_set(): print( self.__name, ':', np.frombuffer( self.__state.get_obj() ) )
        if self.__view_event.is_set(): self.__queue.put( np.frombuffer( self.__state.get_obj() ) )
            
    def __stream(self):
        """ Streams data from the MyoArmband at the specified sampling rate """
        t = time.time()
        while self.__stream_event.is_set():
            t = t + self.__speriod
            self.__read()
            while max( t - time.time(), 0 ): ns_sleep( 1e2 )

    def __plot(self):
        gui = plt.figure()
        gui.canvas.set_window_title( self.__name )
        
        # orientation plots
        orientations = []
        orient_colors = 'rgb'
        orient_titles = [ 'ROLL', 'PITCH', 'YAW' ]
        for i in range( 0, 3 ):
            ax = gui.add_subplot( 2, 3, i + 1, 
                 projection = 'polar', aspect = 'equal' )
            ax.plot( np.linspace( 0, 2*np.pi, 100 ), 
                     np.ones( 100 ), color = orient_colors[ i ],
                     linewidth = 2.0 )
            orientations.append( ax.plot( np.zeros( 2 ), np.linspace( 0, 1, 2 ), 
                                         color = orient_colors[ i ], linewidth = 2.0  ) )
            ax.set_rticks( [] )
            ax.set_rmax( 1 )
            ax.set_xlabel( orient_titles[ i ] )
            ax.grid( True )

        # line plot
        emg_plots = []
        if self.__raw: emg_offsets = np.array( [ 128, 384, 640, 896, 1152, 1408, 1664, 1920 ] )
        else: emg_offsets = np.array( [ 500, 1500, 2500, 3500, 4500, 5500, 6500, 7500 ] )

        ax = gui.add_subplot( 2, 1, 2 )
        num_emg_samps = int( 5 * np.round( 1.0 / self.__speriod ) )
        for i in range( 0, 8 ):
            xdata = np.linspace( 0, 1, num_emg_samps )
            ydata = emg_offsets[ i ] * np.ones( num_emg_samps )
            emg_plots.append( ax.plot( xdata, ydata ) )
        if self.__raw: ax.set_ylim( 0, 2048 )
        else: ax.set_ylim( 0, 9000 )
        ax.set_xlim( 0, 1 )
        ax.set_yticks( emg_offsets.tolist() )
        ax.set_yticklabels( [ 'EMG01', 'EMG02', 'EMG03', 'EMG04', 'EMG05', 'EMG06', 'EMG07', 'EMG08' ] )
        ax.set_xticks( [] )  

        plt.tight_layout()
        plt.show( block = False )
        while self.__view_event.is_set():
            try:
                data = []
                while self.__queue.qsize() > 0: data.append( self.__queue.get() )
                if data:
                    # concate to get a block of data
                    data = np.vstack( data )

                    # update orientation data
                    angles = quat.to_euler( data[-1, 8:] )
                    for i in range( 0, 3 ):
                        tdata = np.ones( 2 ) * angles[ i ]
                        rdata = np.linspace( 0, 1, 2 )
                        orientations[ i ][ 0 ].set_data( tdata, rdata )
                
                    # update electrophysiological data
                    for i in range( 0, 8):
                        ydata = emg_plots[ i ][ 0 ].get_ydata()
                        ydata = np.append( ydata, data[ :, i ] + emg_offsets[ i ] )
                        ydata = ydata[-num_emg_samps:]
                        emg_plots[ i ][ 0 ].set_ydata( ydata )
                plt.pause( 0.005 )
            except: self.__view_event.clear()
        plt.close( gui )

    @property
    def name(self): return self.__name

    @property
    def state(self): return np.frombuffer( self.__state.get_obj() ).copy()

    @property
    def speriod(self): return self.__speriod

    @property
    def channelcount(self): return self.__channelcount

    def run(self, display = False):
        """ Starts the acquisition process of the MyoArmband """
        if not self.__stream_event.is_set():
            if display: self.__print_event.set()
            else: self.__print_event.clear()
            self.__stream_event.set()

    def stop(self):
        '''Stops the acquisition process of the MyoArmband'''
        if self.__stream_event.is_set():
            self.__stream_event.clear()

    def view(self):
        """ Launches the GUI viewer of the MyoArmband """
        if not self.__view_event.is_set():
            self.__view_event.set()
            self.__viewer = mp.Process( target = self.__plot )
            self.__viewer.start()
            
    def hide(self):
        '''Closes the GUI viewer of the MyoArmband'''
        if self.__view_event.is_set():
            self.__view_event.clear()
            self.__viewer.join()

    # NOTE: THE FOLLOWING FUNCTIONS ARE EXPERIMENTAL AND HAVE YET BEEN TESTED!!!
    # def vibrate(self, length):
    #     if length in range( 1, 4 ):
    #         self.__write_attr( 0x19, MyoArmband.pack( '3B', 3, 1, length ) )

if __name__ == '__main__':
    import sys
    import inspect
    import argparse

    # helper function for booleans
    def str2bool( v ):
        if v.lower() in [ 'yes', 'true', 't', 'y', '1' ]: return True
        elif v.lower() in [ 'no', 'false', 'n', 'f', '0' ]: return False
        else: raise argparse.ArgumentTypeError( 'Boolean value expected!' )

    # parse commandline entries
    class_init = inspect.getargspec( MyoArmband.__init__ )
    arglist = class_init.args[1:]   # first item is always self
    defaults = class_init.defaults
    parser = argparse.ArgumentParser()
    for arg in range( 0, len( arglist ) ):
        try: tgt_type = type( defaults[ arg ][ 0 ] )
        except: tgt_type = type( defaults[ arg ] )
        if tgt_type is bool:
            parser.add_argument( '--' + arglist[ arg ], 
                             type = str2bool, nargs = '?',
                             action = 'store', dest = arglist[ arg ],
                             default = defaults[ arg ] )
        else:
            parser.add_argument( '--' + arglist[ arg ], 
                                type = tgt_type, nargs = '+',
                                action = 'store', dest = arglist[ arg ],
                                default = defaults[ arg ] )
    args = parser.parse_args()
    for arg in range( 0, len( arglist ) ):
        attr = getattr( args, arglist[ arg ] )
        if isinstance( attr, list ) and not isinstance( defaults[ arg ], list ):
            setattr( args, arglist[ arg ], attr[ 0 ]  )

    # create interface
    myo = MyoArmband( name = args.name, com = args.com, mac = args.mac, srate = args.srate, raw = args.raw )
    myo.run( display = True )
    myo.view()