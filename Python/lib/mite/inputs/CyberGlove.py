import serial
import struct
import ctypes
import time

import numpy as np
import multiprocessing as mp

from .. import ns_sleep
from . import AbstractBaseInput

class CyberGlove(AbstractBaseInput):
    """ Python implementation of a Wired CyberGlove driver  """
    def __init__(self, name = 'CyberGlove', com = '/dev/ttyUSB0', baud = 115200, srate = 40.0 ):
        """ Constructor """
        self.__name = name
        self.__ser = serial.Serial(com, baud, timeout = 0.5)
        if self.__query_init():
            ch = self.__query_channels()
            if ch > 0:
                # device variables
                self.__channelcount = ch
                self.__state = np.zeros(ch)
                self.__speriod = 1.0 / srate
                self.__state = mp.Array( ctypes.c_double, self.__channelcount )
                
                # streaming variables
                self.__stream_event = mp.Event()
                self.__print_event = mp.Event()
                self.__streamer = None

                # viewing variables
                self.__view_event = mp.Event()
                self.__queue = mp.Queue()
                self.__viewer = None
            else:
                raise ValueError("CyberGlove channel count must be positive", 'ch')
        else:
            raise RuntimeError("CyberGlove was not initialized correctly")

    def __del__(self):
        """ Destructor """
        try:
            if self.__ser.is_open: self.__ser.close()
        except AttributeError: pass # never made the serial device
        try:
            if self.__streamer.is_alive: self.stop()
        except AttributeError: pass # never got to make the I/O process
        try:
            if self.__viewer.is_alive: self.hide()
        except AttributeError: pass # no viewer exists currently

    def __query_init(self):
        """ Checks for proper initialization:
            Returns true, false
        """
        self.__ser.write(b'\x3F\x47')
        init = self.__ser.read(4)
        if len(init) < 4: return False
        else: return (init[2] == 3)

    def __query_channels(self):
        """ Checks for number of channels the CyberGlove has """
        self.__ser.write(b'\x3F\x53')
        ch = self.__ser.read( 4 )
        if len(ch) < 4: return 0
        else: return int( ch[2] )

    def __read(self):
        """ Reads a single sample from the CyberGlove """
        # CyberGlove Sensor Mask
        # 00 Thumb MCP   01 Thumb PIP    02 Thumb DIP   03 Thumb ABD
        # 04 Index MCP   05 Index PIP    06 Index DIP   07 Index ABD
        # 08 Middle MCP  09 Middle PIP   10 Middle DIP  11 Middle ABD
        # 12 Ring MCP    13 Ring PIP     14 Ring DIP    15 Ring ABD
        # 16 Pinky MCP   17 Pinky PIP    18 Pinky DIP   19 Pinky ABD
        # 20 Palm Arch   21 Wrist Pitch  22 Wrist Yaw

        self.__ser.write(b'\x47')
        sample = self.__ser.read( self.__channelcount + 2 )
        self.__state[:] = np.array( struct.unpack( self.__channelcount*'B', sample[1:-1] ) )
        if self.__print_event.is_set(): print( self.__name, ':', np.frombuffer( self.__state.get_obj() ) )
        if self.__view_event.is_set(): self.__queue.put( np.frombuffer( self.__state.get_obj() ) )

    def __stream(self):
        """ Streams data from the CyberGlove at the specified sampling rate """
        t = time.time()
        while self.__stream_event.is_set():
            t = t + self.__speriod
            self.__read()
            while max( t - time.time(), 0 ): ns_sleep( 1e2 )

    @property
    def name(self): return self.__name

    @property
    def state(self): return np.frombuffer( self.__state.get_obj() )

    @property
    def speriod(self): return self.__speriod

    @property
    def channelcount(self): return self.__channelcount

    def run(self, display = False):
        """ Starts the acquisition process of the CyberGlove """
        if not self.__stream_event.is_set():
            self.__stream_event.set()

            if display: self.__print_event.set()
            else: self.__print_event.clear()

            self.__streamer = mp.Process( target = self.__stream )
            self.__streamer.start()

    def stop(self):
        ''' Stops the acquisition process of the CyberGlove '''
        if self.__stream_event.is_set():
            self.__stream_event.clear()
            self.__streamer.join()

    def view(self):
        """ Launches the GUI viewer of the CyberGlove data """
        if not self.__view_event.is_set():
            pass

    def hide(self):
        ''' Closes the GUI viewer of the CyberGlove data '''
        if self.__view_event.is_set():
            self.__view_event.clear()
            self.__viewer.join()

if __name__ == '__main__':
    import sys
    import inspect
    import argparse

    # helper function for booleans
    def str2bool( v ):
        if v.lower() in [ 'yes', 'true', 't', 'y', '1' ]: return True
        elif v.lower() in [ 'no', 'false', 'n', 'f', '0' ]: return False
        else: raise argparse.ArgumentTypeError( 'Boolean value expected!' )

    # parse commandline entries
    class_init = inspect.getargspec( CyberGlove.__init__ )
    arglist = class_init.args[1:]   # first item is always self
    defaults = class_init.defaults
    parser = argparse.ArgumentParser()
    for arg in range( 0, len( arglist ) ):
        try: tgt_type = type( defaults[ arg ][ 0 ] )
        except: tgt_type = type( defaults[ arg ] )
        if tgt_type is bool:
            parser.add_argument( '--' + arglist[ arg ], 
                             type = str2bool, nargs = '?',
                             action = 'store', dest = arglist[ arg ],
                             default = defaults[ arg ] )
        else:
            parser.add_argument( '--' + arglist[ arg ], 
                                type = tgt_type, nargs = '+',
                                action = 'store', dest = arglist[ arg ],
                                default = defaults[ arg ] )
    args = parser.parse_args()
    for arg in range( 0, len( arglist ) ):
        attr = getattr( args, arglist[ arg ] )
        if isinstance( attr, list ) and not isinstance( defaults[ arg ], list ):
            setattr( args, arglist[ arg ], attr[ 0 ]  )

    # create interface

    cg = CyberGlove( name = args.name, com = args.com, baud = args.baud, srate = args.srate )
    cg.run( display = True )
    cg.view()
