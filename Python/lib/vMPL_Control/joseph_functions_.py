import numpy as np
import math, sys
from mite.utils import Quaternion as quat
import matplotlib.pyplot as plt
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting/vMPL_Control' )
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting/vMPL_Control' )
from mpl import JointEnum as MplId
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import *

### Function Definitions ##########################
###################################################
def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    # classes = classes[unique_labels(y_true, y_pred)]
    classes = np.unique(y_true)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax



###  returns LSTM formatted data: 2D stream of data (num_samples x num_features) to 3D (num_samples-seq_length x seq_length x input_dim/num_features)
def lstm_fix_data(data, labels, seq_length): ##
    tempX, tempY = [], []
    for i in range(0,len(data)-seq_length,1):        
        tempX.append(data[i:i+seq_length,:])
        tempY.append(labels[i+seq_length,:])
    fixX = np.array(tempX)
    fixY = np.array(tempY)
    return fixX, fixY

### convert a scaler (in {-1,0,1}) to one-hot (eg. [0 1 0])
def dof_encode(dof, thresh):
    if dof < -thresh:
        encoded_label = [0, 1, 0]
    elif dof > thresh: 
        encoded_label = [0, 0, 1]
    else:
        encoded_label = [1, 0, 0]
    return encoded_label

### convert a scaler (in [-1,1]) to one-hot regression output (eg. [0 0.61 0])
def dof_encode_regression(dof, thresh):
    if dof < -thresh:
        encoded_label = [0, 1, 0]
    elif dof > thresh: 
        encoded_label = [0, 0, 1]
    else:
        encoded_label = [1, 0, 0]
    # return encoded_label

### convert one-hot (eg. [0 1 0]) to a scaler (in {-1,0,1})
def dof_decode(encoded_label):
    idx = encoded_label.argmax(axis=0)
    if idx == 1:
        decoded_label = 0
    elif idx == 2: 
        decoded_label = 2
    else:
        decoded_label = 1
    return decoded_label

### convert softmax input (eg. [0.2 0.61 0.19]) to a scaler (in [-1,1])
def dof_decode_regression(encoded_label):
    idx = encoded_label.argmax(axis=0)
    # if idx == 1:
    #     decoded_label = -1
    # elif idx == 2: 
    #     decoded_label = 1
    # else:
    #     decoded_label = 0
    # return decoded_label

### convert a scaler (in {-1,0,1}) to one-hot (eg. [0 1 0]) for many samples at once
def encode_datablock(label_block, vel_threshold):
    encoded_data = []
    for i in range(label_block.shape[0]):
        arr = label_block[i,:]
        arr_encoded = np.hstack( (dof_encode(arr[0], vel_threshold), dof_encode(arr[1], vel_threshold), dof_encode(arr[2], vel_threshold)) )   
        encoded_data.append( arr_encoded )
    return np.vstack( encoded_data )

### convert one-hot (eg. [0 1 0]) to a scaler (in {-1,0,1}) for many samples at once
def decode_datablock(encoded_label_block):
    decoded_data = []
    for i in range(encoded_label_block.shape[0]):
        arr = encoded_label_block[i,:]
        arr_decoded = []
        dofs = encoded_label_block.shape[1]//3
        for i in range(0,dofs):
            arr_decoded.append( dof_decode(arr[i*3:i*3+3]) )    

        arr_decoded = np.hstack(arr_decoded)
        decoded_data.append( arr_decoded )
    return np.vstack( decoded_data )

### per-DoF accuracy from a sequence of predictions, currently expects a 6-element vector representing 2 dofs
def get_3dof_accuracy(actual, predicted, majority_filter_length):
    predicted1 = np.squeeze(np.reshape(predicted[:,0],(1,-1))).tolist()    
    predicted1 = np.squeeze(majority_filter(predicted1, majority_filter_length))
    predicted2 = np.squeeze(np.reshape(predicted[:,1],(1,-1))).tolist()    
    predicted2 = np.squeeze(majority_filter(predicted2, majority_filter_length))
    predicted3 = np.squeeze(np.reshape(predicted[:,2],(1,-1))).tolist()    
    predicted3 = np.squeeze(majority_filter(predicted3, majority_filter_length))
    actual1 = np.squeeze(np.reshape(actual[:,0],(1,-1))).tolist()
    actual1 = np.squeeze(majority_filter(actual1, majority_filter_length))
    actual2 = np.squeeze(np.reshape(actual[:,1],(1,-1))).tolist()
    actual2 = np.squeeze(majority_filter(actual2, majority_filter_length))   
    actual3 = np.squeeze(np.reshape(actual[:,2],(1,-1))).tolist()
    actual3 = np.squeeze(majority_filter(actual3, majority_filter_length))
    dof1_matches = actual1==predicted1
    dof2_matches = actual2==predicted2
    dof3_matches = actual3==predicted3
    acc1 = np.mean(dof1_matches)*100
    acc2 = np.mean(dof2_matches)*100
    acc3 = np.mean(dof3_matches)*100
    # accTotal = np.mean(dof1_matches==dof2_matches)*100
    return [acc1, acc2, acc3]

### per-DoF accuracy from a sequence of predictions, currently expects a 6-element vector representing 2 dofs
def get_2dof_accuracy(actual, predicted, majority_filter_length):
    predicted1 = np.squeeze(np.reshape(predicted[:,0],(1,-1))).tolist()    
    predicted1 = np.squeeze(majority_filter(predicted1, majority_filter_length))
    predicted2 = np.squeeze(np.reshape(predicted[:,1],(1,-1))).tolist()    
    predicted2 = np.squeeze(majority_filter(predicted2, majority_filter_length))
    actual1 = np.squeeze(np.reshape(actual[:,0],(1,-1))).tolist()
    actual1 = np.squeeze(majority_filter(actual1, majority_filter_length))
    actual2 = np.squeeze(np.reshape(actual[:,1],(1,-1))).tolist()
    actual2 = np.squeeze(majority_filter(actual2, majority_filter_length)) 
    dof1_matches = actual1==predicted1
    dof2_matches = actual2==predicted2
    acc1 = np.mean(dof1_matches)*100
    acc2 = np.mean(dof2_matches)*100
    # accTotal = np.mean(dof1_matches==dof2_matches)*100
    return [acc1, acc2]

### filter a prediction stream to compute accuracy, input width=1 for no filter
def majority_filter(seq, width):
    offset = width // 2
    seq = [0] * offset + seq
    result = []
    for i in range(len(seq) - offset):
        a = seq[i:i+width]
        result.append(max(set(a), key=a.count))
    return np.squeeze(result)

### softmax applied to vector x
def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()

### sigmoid applied to vector x
def sigmoid(x):
    return [1 / (1 + math.exp(-n)) for n in x]

### converts imu.state quaternions into joint angle vector
def get_angles(state):
    num_quat = int( len( state ) / 4 )
    angles = []
    for q in range( 1, num_quat ):
        idx1 = q * 4 - 4
        idx2 = q * 4
        q1 = state[ idx1:idx1+4 ]
        q2 = state[ idx2:idx2+4 ]
        q12 = quat.multiply( quat.inverse( q1 ), q2 )
        euler_angles = quat.quaternion_to_euler_angle( q12 )
        angles.append( euler_angles )
        # print( np.hstack(angles)*180/math.pi, flush=True )
    return angles

class ScaleCyberglove():
    def __init__(self):
        self.hand_flex_scaler = MinMaxScaler(feature_range=(-0.52, 1.57)) # vMPL MCP joint range (flexion-extension): -30deg - 110deg
        self.wrist_flex_scaler = MinMaxScaler(feature_range=(-0.79, 1.05))
        self.wrist_add_scaler = MinMaxScaler(feature_range=(-0.52, 0.52))

    def calibrate(self, cal_data=[]):
        self.hand_flex_scaler = self.hand_flex_scaler.fit(np.reshape(cal_data[:,7],[-1,1]))
        self.wrist_flex_scaler = self.wrist_flex_scaler.fit(np.reshape(cal_data[:,20],[-1,1]))   
        self.wrist_add_scaler = self.wrist_add_scaler.fit(np.reshape(cal_data[:,21],[-1,1])) 

    def scale(self, cyber_state=[] ):
        hand_flex = self.hand_flex_scaler.transform(np.reshape(cyber_state[7],[-1,1])) 
        wrist_flex = self.wrist_flex_scaler.transform(np.reshape(cyber_state[20],[-1,1]))
        wrist_add = self.wrist_add_scaler.transform(np.reshape(cyber_state[21],[-1,1]))

        cyber_angles = [hand_flex, wrist_flex, wrist_add]
    
        return cyber_angles


def get_pos( angles ):
    if len( angles ) == 2:  
        pos = np.zeros(3)
        pos[0] = -angles[0][0] # wrist_flex
        pos[1] = -angles[0][2] # wrist_adduct
        pos[2] = -angles[1][0] # hand_flex
    elif len( angles ) == 4:
        pos = np.zeros(6)
        pos[0] = -angles[0][0] # shoulder_flex
        pos[1] = -angles[0][2] # shoulder_adduct
        pos[2] = -angles[1][0] # elbow_flex
        pos[3] = -angles[0][0] # wrist_flex
        pos[4] = -angles[0][2] # wrist_adduct
        pos[5] = -angles[1][0] # hand_flex

    return pos

### preps joint angle vector from IMUs (via get_angles()) into acceptable vMPL format
def set_vMPL_angles( imu_angles=[], cyber_angles=[], IMU_arm='right' ):
    if len( imu_angles ) == 3 and cyber_angles == []: #assumes only wrist (w/ rotation) and hand angles are needed
        shoulder_flex = 0
        shoulder_adduct = 0
        humeral_rotate = 0 
        elbow_flex = 1.57
        wrist_rotate = -imu_angles[0][1]  
        wrist_flex = -imu_angles[1][0]
        wrist_adduct = -imu_angles[1][2]
        hand_flex = -imu_angles[2][0]

    elif len( imu_angles ) == 3 and cyber_angles != []:
        shoulder_flex = imu_angles[0][2]
        shoulder_adduct = -imu_angles[0][0]
        humeral_rotate = 0
        elbow_flex = imu_angles[1][2]
        wrist_rotate = -imu_angles[2][1]  
        wrist_flex = cyber_angles[1]
        wrist_adduct = cyber_angles[2]
        hand_flex = cyber_angles[0]         
    
    elif len( imu_angles ) == 5 and cyber_angles == []: #assumes shoulder, elbow, wrist, and hand angles are needed
        shoulder_flex = imu_angles[0][2]
        shoulder_adduct = -imu_angles[0][0]
        humeral_rotate = 0
        elbow_flex = imu_angles[1][2]
        wrist_rotate = -imu_angles[2][1]  
        wrist_flex = -imu_angles[3][0]
        wrist_adduct = -imu_angles[3][2]
        hand_flex = -imu_angles[4][0]

    elif imu_angles == [] and cyber_angles != []:
        shoulder_flex = 0
        shoulder_adduct = 0
        humeral_rotate = 0 
        elbow_flex = 1.57
        wrist_rotate = 0    
        wrist_flex = cyber_angles[1]
        wrist_adduct = cyber_angles[2]
        hand_flex = cyber_angles[0]      

    if IMU_arm == 'left': #set angles for left arm
        wrist_rotate = -wrist_rotate
        wrist_adduct = -wrist_adduct
    
    arm_angles = [0.0] * 27
    arm_angles[MplId.SHOULDER_FE] = shoulder_flex 
    arm_angles[MplId.SHOULDER_AB_AD] = shoulder_adduct
    arm_angles[MplId.HUMERAL_ROT] = humeral_rotate
    arm_angles[MplId.ELBOW] = elbow_flex 
    arm_angles[MplId.WRIST_ROT] = wrist_rotate
    arm_angles[MplId.WRIST_FE] = wrist_flex
    arm_angles[MplId.WRIST_AB_AD] = wrist_adduct    
    hand_idx = [MplId.INDEX_MCP, MplId.MIDDLE_MCP, MplId.RING_MCP, MplId.LITTLE_MCP]
    pip_idx = [MplId.INDEX_PIP, MplId.MIDDLE_PIP, MplId.RING_PIP, MplId.LITTLE_PIP, MplId.INDEX_DIP, MplId.MIDDLE_DIP, MplId.RING_DIP, MplId.LITTLE_DIP]
    thumb_idx = [MplId.THUMB_CMC_FE, MplId.THUMB_MCP, MplId.THUMB_DIP]
    arm_angles[MplId.THUMB_CMC_AB_AD] = 0.5 * hand_flex
    for idx in hand_idx:
        arm_angles[idx] = hand_flex
    for idx in pip_idx:
        arm_angles[idx] = 1.0 * hand_flex
    for idx in thumb_idx:
        arm_angles[idx] = 0.6 * hand_flex
    return arm_angles

### preps joint angle vector from IMUs (via get_angles()) into acceptable vMPL format
def set_vMPL_angles2( imu_angles=[], cyber_angles=[], IMU_arm='right', IMU_locations=['N/A','N/A','N/A','N/A','N/A','N/A'] ):
    if IMU_locations[0]=='Chest' and IMU_locations[1]=='Shoulder':
        shoulder_flex = imu_angles[0][2]
        shoulder_adduct = -imu_angles[0][0]
        humeral_rotate = 0 
    else:
        shoulder_flex = 0
        shoulder_adduct = 0
        humeral_rotate = 0 
        
    if IMU_locations[0]=='Shoulder' and IMU_locations[1]=='Elbow':  
        elbow_flex = imu_angles[0][2]
    elif IMU_locations[1]=='Shoulder' and IMU_locations[2]=='Elbow':
        elbow_flex = imu_angles[1][2]
    else:
        elbow_flex = 1.57
    
    if IMU_locations[0]=='Elbow' and IMU_locations[1]=='Wrist':  
        wrist_rotate = -imu_angles[0][1]
    elif IMU_locations[1]=='Elbow' and IMU_locations[2]=='Wrist':
        wrist_rotate = -imu_angles[1][1]
    elif IMU_locations[2]=='Elbow' and IMU_locations[3]=='Wrist':
        wrist_rotate = -imu_angles[2][1]
    else:
        wrist_rotate = 0

    if IMU_locations[0]=='Wrist' and IMU_locations[1]=='Dorsum':  
        wrist_flex = imu_angles[0][0]
        wrist_adduct = imu_angles[0][2]
    elif IMU_locations[1]=='Wrist' and IMU_locations[2]=='Dorsum':
        wrist_flex = imu_angles[1][0]
        wrist_adduct = imu_angles[1][2]
    elif IMU_locations[2]=='Wrist' and IMU_locations[3]=='Dorsum':
        wrist_flex = imu_angles[2][0]
        wrist_adduct = imu_angles[2][2]
    elif IMU_locations[3]=='Wrist' and IMU_locations[4]=='Dorsum':
        wrist_flex = imu_angles[3][0]
        wrist_adduct = imu_angles[3][2]
    else:
        wrist_flex = cyber_angles[1]
        wrist_adduct = cyber_angles[2]

    if IMU_locations[0]=='Dorsum' and IMU_locations[1]=='Fingers':  
        hand_flex = -imu_angles[0][0]
    elif IMU_locations[1]=='Dorsum' and IMU_locations[2]=='Fingers':
        hand_flex = -imu_angles[1][0]
    elif IMU_locations[2]=='Dorsum' and IMU_locations[3]=='Fingers':
        hand_flex = -imu_angles[2][0]
    elif IMU_locations[3]=='Dorsum' and IMU_locations[4]=='Fingers':
        hand_flex = -imu_angles[3][0]
    elif IMU_locations[4]=='Dorsum' and IMU_locations[5]=='Fingers':
        hand_flex = -imu_angles[4][0]
    else:
        hand_flex = cyber_angles[0]

    if IMU_arm == 'left': #set angles for left arm
        if 'Elbow' in IMU_locations and 'Wrist' in IMU_locations:
            wrist_rotate = -wrist_rotate
        if 'Wrist' in IMU_locations and 'Dorsum' in IMU_locations:
            wrist_adduct = -wrist_adduct
    

    arm_angles = [0.0] * 27
    arm_angles[MplId.SHOULDER_FE] = shoulder_flex 
    arm_angles[MplId.SHOULDER_AB_AD] = shoulder_adduct
    arm_angles[MplId.HUMERAL_ROT] = humeral_rotate
    arm_angles[MplId.ELBOW] = elbow_flex 
    arm_angles[MplId.WRIST_ROT] = wrist_rotate
    arm_angles[MplId.WRIST_FE] = wrist_flex
    arm_angles[MplId.WRIST_AB_AD] = wrist_adduct    
    hand_idx = [MplId.INDEX_MCP, MplId.MIDDLE_MCP, MplId.RING_MCP, MplId.LITTLE_MCP]
    pip_idx = [MplId.INDEX_PIP, MplId.MIDDLE_PIP, MplId.RING_PIP, MplId.LITTLE_PIP, MplId.INDEX_DIP, MplId.MIDDLE_DIP, MplId.RING_DIP, MplId.LITTLE_DIP]
    thumb_idx = [MplId.THUMB_CMC_FE, MplId.THUMB_MCP, MplId.THUMB_DIP]
    arm_angles[MplId.THUMB_CMC_AB_AD] = 0.5 * hand_flex
    for idx in hand_idx:
        arm_angles[idx] = hand_flex
    for idx in pip_idx:
        arm_angles[idx] = 1.0 * hand_flex
    for idx in thumb_idx:
        arm_angles[idx] = 0.6 * hand_flex
    
    return arm_angles


def output_class_to_angles(classnum):
    shoulder_flex = 0
    shoulder_adduct = 0
    humeral_rotate = 0 
    elbow_flex = 1.57
    wrist_rotate = 0

    arm_angles = [0.0] * 27
    arm_angles[MplId.SHOULDER_FE] = shoulder_flex 
    arm_angles[MplId.SHOULDER_AB_AD] = shoulder_adduct
    arm_angles[MplId.HUMERAL_ROT] = humeral_rotate
    arm_angles[MplId.ELBOW] = elbow_flex 

    # self.hand_flex_scaler = MinMaxScaler(feature_range=(-0.52, 1.57)) # vMPL MCP joint range (flexion-extension): -30deg - 110deg
    # self.wrist_flex_scaler = MinMaxScaler(feature_range=(-0.79, 1.05))
    # self.wrist_add_scaler = MinMaxScaler(feature_range=(-0.52, 0.52))

    ternary_classnum = dec2ternary( classnum )
    # print(ternary_classnum, flush=True)

    if ternary_classnum[0] == 0:
        hand_flex = -0.52
    elif ternary_classnum[0] == 2:
        hand_flex = 1.57
    else:
        hand_flex = 0.5

    if ternary_classnum[1] == 0:
        wrist_flex = -0.79
    elif ternary_classnum[1] == 2:
        wrist_flex = 1.05
    else:
        wrist_flex = 0

    if ternary_classnum[2] == 0:
        wrist_adduct = -0.52
    elif ternary_classnum[2] == 2:
        wrist_adduct = 0.52
    else:
        wrist_adduct = 0    

    arm_angles[MplId.WRIST_ROT] = wrist_rotate
    arm_angles[MplId.WRIST_FE] = wrist_flex
    arm_angles[MplId.WRIST_AB_AD] = wrist_adduct    
    hand_idx = [MplId.INDEX_MCP, MplId.MIDDLE_MCP, MplId.RING_MCP, MplId.LITTLE_MCP]
    pip_idx = [MplId.INDEX_PIP, MplId.MIDDLE_PIP, MplId.RING_PIP, MplId.LITTLE_PIP, MplId.INDEX_DIP, MplId.MIDDLE_DIP, MplId.RING_DIP, MplId.LITTLE_DIP]
    thumb_idx = [MplId.THUMB_CMC_FE, MplId.THUMB_MCP, MplId.THUMB_DIP]
    arm_angles[MplId.THUMB_CMC_AB_AD] = 0.5 * hand_flex
    for idx in hand_idx:
        arm_angles[idx] = hand_flex
    for idx in pip_idx:
        arm_angles[idx] = 1.0 * hand_flex
    for idx in thumb_idx:
        arm_angles[idx] = 0.6 * hand_flex
    return arm_angles


    
def position_cap_and_flag(pos,thresh,posMin,posMax):
    dim = pos.shape[0]
    pos_cap = np.zeros(dim)
    pos_flag = np.zeros(dim)
    for i in range(0,dim):
        a = pos[i]
        z = 0
        if a>thresh*posMax[i]: z = 1
        if a<thresh*posMin[i]: z = -1
        if a>thresh*posMax[i]: a = thresh*posMax[i]    
        if a<thresh*posMin[i]: a = thresh*posMin[i]   
        pos_cap[i] = a
        pos_flag[i] = z
    return pos_cap, pos_flag


def cap_and_flag_datablock(datablock,thresh,posMin,posMax):
    dim = datablock.shape[0]
    cap_block = np.zeros(datablock.shape)
    flag_block = np.zeros(datablock.shape)
    for i in range(0,dim):
        cap_block[i,:], flag_block[i,:] = position_cap_and_flag(datablock[i,:], thresh, posMin, posMax)
    return cap_block, flag_block


### convert a scaler (in {-1,0,1}) to one-hot (eg. [0 1 0])
def dof_encode_pos_classify(dof, posMin, posMax, posRest, tolerance):
    if dof < ( posRest - tolerance*abs(posRest-posMin) ): # (-) move DoF
        encoded_label = [0, 1, 0]
    elif dof > ( posRest + tolerance*abs(posMax-posRest) ): # (+) move DoF
        encoded_label = [0, 0, 1]
    else: # rest DoF
        encoded_label = [1, 0, 0]
    return encoded_label


### convert a scaler (in {-1,0,1}) to one-hot (eg. [0 1 0]) for many samples at once
def encode_datablock_pos_classify(label_block, posMin, posMax, posRest, tolerance):
    encoded_data = []
    for i in range(label_block.shape[0]):
        arr = label_block[i,:]
        arr_encoded = np.hstack( (dof_encode_pos_classify(arr[0], posMin[0], posMax[0], posRest[0], tolerance), \
            dof_encode_pos_classify(arr[1], posMin[1], posMax[1], posRest[1], tolerance), \
            dof_encode_pos_classify(arr[2], posMin[2], posMax[2], posRest[2], tolerance)) )   
        encoded_data.append( arr_encoded )
    return np.vstack( encoded_data )


### convert a scaler (in {-1,0,1}) to one-hot (eg. [0 1 0]) for many samples at once
def encode_7classes(encoded_labels):
    class_labels = []
    for i in range(encoded_labels.shape[0]):
        arr = encoded_labels[i,:]
        if   np.array_equal( arr, [1,1,1] ):
            label = 0 # REST
        elif arr[0] == 2:
            label = 1 # HAND CLOSE
        elif np.array_equal( arr, [0,1,1] ):
            label = 2 # HAND OPEN
        elif np.array_equal( arr, [1,2,1] ) or np.array_equal( arr,[0,2,1] ):
            label = 3 # WRIST FLEX
        elif np.array_equal( arr, [1,0,1] ) or np.array_equal( arr,[0,0,1] ):
            label = 4 # WRIST EXTEND
        elif np.array_equal( arr, [1,1,2] ) or np.array_equal( arr,[0,1,2] ):
            label = 5 # WRIST DOWN
        elif np.array_equal( arr,[1,1,0] ) or np.array_equal( arr,[0,1,0] ):
            label = 6 # WRIST UP
        else:
            label = 0 # REST
        class_labels.append( label )
    return np.vstack( class_labels )

def encode_27classes(encoded_labels):
    class_labels = []
    for i in range(encoded_labels.shape[0]):
        arr = encoded_labels[i,:]
        str0 = str(arr[0])
        str1 = str(arr[1])
        str2 = str(arr[2])
        ternary_num = int(str0+str1+str2)
        label = other2dec(ternary_num,3)
        class_labels.append( label )
    return np.vstack( class_labels )

def other2dec(n, other):
    return sum([(int(v) * other**i) for i, v in enumerate(list(str(n))[::-1])])

def dec2ternary(n):    
    arr = [0,0,0]
    i = 2
    while n:
        n, arr[i] = divmod(n, 3)
        i = i-1   
    return arr


### per-DoF accuracy from a sequence of predictions, currently expects a 6-element vector representing 2 dofs
def get_dof_accuracy(actual, predicted, majority_filter_length):
    predicted = np.squeeze(np.reshape(predicted[:,0],(1,-1))).tolist()    
    predicted = np.squeeze(majority_filter(predicted, majority_filter_length))    
    actual = np.squeeze(np.reshape(actual[:,0],(1,-1))).tolist()
    actual = np.squeeze(majority_filter(actual, majority_filter_length))    
    dof_matches = actual==predicted   
    acc = np.mean(dof_matches)*100
    return acc, dof_matches


def smooth(x,window_len):
    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")
    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")
    if window_len<3:
        return x
    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    w=np.hanning(window_len)
    y=np.convolve(w/w.sum(),s,mode='valid')
    return y
