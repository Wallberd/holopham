## vMPL Virtual Environment console must be open 
import sys
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting' )
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' )
from mpl.unity import UnityUdp
from mpl import JointEnum as MplId
import math, time, copy
import scipy.io as sio
import numpy as np
np.set_printoptions( precision = 3 )
from mite.inputs import IntanRHD2000
from mite.filters import TimeDomainFilter

def filter_raw_data(rawData, rawLabels):
    data, feats, labels = [], [], []
    main_counter, short_counter = 1,1
    for i in range(len(rawLabels)):
        data.append( rawData[i,:] )          
        ## grab last 'window' samples and compute td5 features if a step-size has passed
        if main_counter>=window and short_counter>=stepsize: 
            tempdata = np.vstack( data[main_counter-window:main_counter] )
            current_feature_vector = td5.filter( tempdata )
            feats.append( current_feature_vector )  
            labels.append( rawLabels.item( (i,0) ) )
            short_counter = 0
        main_counter += 1
        short_counter += 1
    Xdata = np.vstack( feats )
    ylabels = np.vstack( labels )
    return (Xdata, ylabels)

myo_arm = ['right','left'][0]
classes = { 'rest': [0.0, 0.2, 0.0, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 1.57, 1.1, 0.25, 0.4], 
            'hand open': [0.0, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            'hand closed': [0.0, 0.0, 0.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.2, 0.75, 0.75, 1.0],
            'finger point': [0.0, 0.0, 0.0, 1.0, 0.0, 0.1, 0.1, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.2, 0.75, 0.75, 1.0] }
upper_arm_angles = [0.0, 0.0, 0.0, 1.57]
lower_arm_angles = [0.0] * 23
num_loops = 3
hold_time = 5
emgSamplingRate = 17
savefile = '/home/jtkrall/Documents/onlinetesting/rnn_experiments/offline_multi_dof/data/hd_emg_4class_data'

### MAIN code ##########################
print( "Initializing HD-EMG...", flush=True )
board = IntanRHD2000()
board.run()
print( "HD-EMG initialized.", flush=True )


## Configure which hands/arms
if myo_arm == 'right': # set up for RIGHT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25000', local_address='//0.0.0.0:25001')
else: # set up for LEFT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25100', local_address='//127.0.0.1:25101')
print('Configured arm...', flush=True)

## Open hand/arm connections
sArm.connect()
print('Opened arm connection...', flush=True)

rawMyoData, labels = [], []
time.sleep(2)

for loop in range( num_loops ):
    t0 = time.time()
    class_idx = 0
    for key in classes:
        print(key, flush=True)
        
        main_counter, short_counter = 1,1
        sys.stdout.flush()
        
        ## Output arm angles to vMPL via UDP
        lower_arm_angles = classes[key]
        sArm.send_joint_angles(upper_arm_angles + lower_arm_angles)
        time.sleep(1/50) # allows arm time to actually get to new position
        
        t0 = time.time()
        while time.time()-t0 <= hold_time:
            #rawMyoData.append( 0 )
            rawMyoData.append( board.state )
            for i in range(60):
                labels.append( class_idx )
            time.sleep(1/emgSamplingRate)
            
        class_idx += 1

sArm.send_joint_angles( [0.0]*27 )
sArm.close()
board.stop()

print('Prepping Data... ', flush=True)
rawEMG = np.vstack( rawMyoData )
labels = np.vstack( labels )

print('Preforming Feature Extraction... ', flush=True)
stepsize, window = 60, 100
td5 = TimeDomainFilter( eps_zc = 1e-6, eps_ssc = 1e-6 )
(X, Y) = filter_raw_data(rawEMG, labels)

print('Saving Data... ', flush=True)
sio.savemat(savefile+'.mat', dict([('rawEMG', rawEMG),('labels', labels)]))
sio.savemat(savefile+'_td5_win'+str(window)+'step'+str(stepsize)+'.mat', dict([('X', X),('Y', Y)]))