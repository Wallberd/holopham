## vMPL Virtual Environment console must be open 
import sys
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting' )
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' )
from mite.inputs import MyoArmband

### MAIN code ##########################
print( "Initializing myo...", flush=True )
# myo = MyoArmband( mac = 'ff:f5:c9:fc:bc:17' ) 
myo = MyoArmband( mac = 'eb:33:40:96:ce:a5' )
# myo = MyoArmband( com = '/dev/ttyACM0', mac = 'e0:f2:99:e7:60:40' )
# myo = MyoArmband( mac = 'd2:8c:41:0e:05:33' )
myo.run( display = False )
print( "Myo initialized.", flush=True )

myo.view()