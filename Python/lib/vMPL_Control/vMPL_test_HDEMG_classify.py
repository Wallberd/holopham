import sys, time
import scipy.io as sio
import numpy as np
np.set_printoptions( precision = 2, threshold=np.inf)
import utils
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler

 # local/personal packages and functions
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' )
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting/vMPL_Control' )
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting' )
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting/vMPL_Control' )
from joseph_functions import *
import rnn_models as rnn
from keras.utils import to_categorical, np_utils
from mite.filters import TimeDomainFilter
import random
random.seed(1)

from mpl.unity import UnityUdp
from mpl import JointEnum as MplId
from mite.inputs import IntanRHD2000

### User Vars ##########################
stepsize = 60
batch_size, sub_epochs = 100, 40
moving_avg_length = 3
majority_filter_length = 1
train_pct = 0.66
emgSamplingRate = 17
test_time = 100
myo_arm = ['right','left'][0]
causal = [False, True][1]
stateful_Enable = [False, True][0]
verbosity = 2
num_classes = 4
#num_classes = 18
MAV_only = [False, True][1]
if MAV_only:
    features = 'mav'
else:
    features = 'td5'

classes = { 'rest': [0.0, 0.2, 0.0, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 1.57, 1.1, 0.25, 0.4], 
            'hand open': [0.0, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            'hand closed': [0.0, 0.0, 0.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.2, 0.75, 0.75, 1.0],
            'finger point': [0.0, 0.0, 0.0, 1.0, 0.0, 0.1, 0.1, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.2, 0.75, 0.75, 1.0] }
upper_arm_angles = [0.0, 0.0, 0.0, 1.57]
lower_arm_angles = [0.0] * 23

mav_indeces = np.arange( 0, 640, 5 ) # index of 8 MAV features/ch within TD5 (40dim)
#mav_indeces = np.arange( 0, 60, 5 ) # index of 8 MAV features/ch within TD5 (40dim)
conv = 5
lstm_nodes = 64
n_nodes = [128, 288]
model_type = ["BLSTM", "TCNN",  "ED-TCN", "TDNN"][2]

if model_type == "BLSTM" and features == 'mav':
    window, seq_length, sub_epochs = 40, 3, 35
if model_type == "TCNN" and features == 'mav':
    window, seq_length, sub_epochs = 40, 60, 35
if model_type == "ED-TCN" and features == 'mav':
    window, seq_length, sub_epochs = 100, 32, 10
if model_type == "TDNN" and features == 'mav':
    window, seq_length, sub_epochs = 20, 60, 35
  
if model_type == "BLSTM" and features == 'td5':
    window, seq_length, sub_epochs = 35, 12, 40
if model_type == "TCNN" and features == 'td5':
    window, seq_length, sub_epochs = 40, 60, 40
if model_type == "ED-TCN" and features == 'td5':
    window, seq_length, sub_epochs = 35, 8, 40
if model_type == "TDNN" and features == 'td5':
    window, seq_length, sub_epochs = 25, 28, 40
max_len = seq_length

if model_type == 'TCNN':
    causal = False
else:
    causal = True

td5 = TimeDomainFilter( eps_zc = 1e-6, eps_ssc = 1e-6 )

### Train neural network w/ pre-collected user data ##########################
#fileData = sio.loadmat('/home/jtkrall/Documents/onlinetesting/rnn_experiments/offline_multi_dof/data/hd_emg_4class_data.mat')
#rawEMG = fileData['rawEMG']
#labels = fileData['labels']
fileData = sio.loadmat('/home/jtkrall/Documents/onlinetesting/rnn_experiments/offline_multi_dof/data/hd_emg_4class_data_td5_win100step60.mat')
X = fileData['X']
Y = fileData['Y']

if MAV_only:
    X = X[:,mav_indeces]

Y_OneHot = [np_utils.to_categorical(y, num_classes) for y in Y]

## Scale features
# scaler = MinMaxScaler(feature_range=(0, 1))
scaler = RobustScaler()
scaler = scaler.fit(X)
X = scaler.transform(X)
num_features = X[1].shape[0]

X_m, sample = [], []
for idx in range(X.shape[0]-(seq_length-1)):
    sample = X[ idx:idx+seq_length , : ]
    X_m.append(sample)
X_m = np.stack(X_m, axis=0)


Y_OneHot_m, label_sample = [], []
for idx in range(len(Y_OneHot)-(seq_length-1)):
    label_sample = np.vstack( Y_OneHot[ idx:idx+seq_length ] )
    Y_OneHot_m.append(label_sample)
Y_OneHot_m = np.stack(Y_OneHot_m, axis=0)

M_train = np.ones( [X_m.shape[0],X_m.shape[1],1] )


 ## Set up sequential model
print('Building model...', model_type, flush=True)
if model_type == "BidirLSTM":
    model, param_str = rnn.BidirLSTM(n_nodes[0], num_classes, num_features, causal=causal, return_param_str=True)
    model.fit(X_m, Y_OneHot_m, epochs=sub_epochs, batch_size=batch_size, verbose=verbosity, sample_weight=M_train[:,:,0]) 
if model_type == "ED-TCN":
    model, param_str = rnn.ED_TCN(n_nodes, conv, num_classes, num_features, max_len, causal=causal, activation='norm_relu', return_param_str=True) 
    model.fit(X_m, Y_OneHot_m, epochs=sub_epochs, batch_size=batch_size, verbose=verbosity, sample_weight=M_train[:,:,0]) 
if model_type == "TDNN":
    model, param_str = rnn.TimeDelayNeuralNetwork(n_nodes, conv, num_classes, num_features, max_len, causal=causal, activation='tanh', return_param_str=True)
    model.fit(X_m, Y_OneHot_m, epochs=sub_epochs, batch_size=batch_size, verbose=verbosity, sample_weight=M_train[:,:,0]) 
if model_type == "tCNN":
    model, param_str = rnn.temporal_convs_linear(n_nodes[0], conv, num_classes, num_features, max_len, causal=causal, return_param_str=True)
    model.fit(X_m, Y_OneHot_m, epochs=sub_epochs, batch_size=batch_size, verbose=verbosity, sample_weight=M_train[:,:,0]) 

 # predict code-- this first instance just predicts on the training data, not recorded
#y_pred = model.predict(X_test_m, verbose=2)
#y_pred = utils.unmask(y_pred, M_test)
#print(np.argmax(y_pred[:,seq_length-1,:]))



### Online testing protocol ##########################
print( "Initializing HD-EMG...", flush=True )
board = IntanRHD2000()
board.run()
print( "HD-EMG initialized.", flush=True )

## Configure which hands/arms
if myo_arm == 'right': # set up for RIGHT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25000', local_address='//0.0.0.0:25001')
else: # set up for LEFT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25100', local_address='//127.0.0.1:25101')
print('Configured arm...', flush=True)

## Open hand/arm connections
sArm.connect()
print('Opened arm connection...', flush=True)

## Initialize streaming EMG buffer
rawTest, featureTestData = [], []
for i in range((window//60)+1):
    rawTest.append( board.state )
    time.sleep(1/emgSamplingRate)

## Main online loop
short_counter = 0
t0 = time.time()
while time.time()-t0 <= test_time:
    ## Collect new EMG samples and delete oldest samples
    rawTest.append( board.state )
    time.sleep(1/emgSamplingRate)
    if len(rawTest) > (window//60)+1:
        del rawTest[0]
    short_counter += 1
    
    ## When required number of new samples are collected, extract new feature vector and delete oldest one
    if short_counter*60 == stepsize:
        tempdata = np.vstack( rawTest )[:,:128]
        current_feature_vector = td5.filter( tempdata[-window:,:] )
        if MAV_only:
            current_feature_vector = current_feature_vector[ mav_indeces ]
        featureTestData.append( current_feature_vector )
        if len(featureTestData) > seq_length:
            del featureTestData[0]
        
        ## Prepare data and make prediction
        if len(featureTestData) == seq_length:
            Xdata = np.vstack( featureTestData )
            Xdata = scaler.transform( Xdata )
            Xdata = np.expand_dims(Xdata, axis=0)
            y_pred = model.predict(Xdata, verbose=2)
            print(np.argmax(y_pred[:,seq_length-1,:]))
            
            ## Send predicted classification to vMPL
            if np.argmax(y_pred[:,seq_length-1,:]) == 0:
                lower_arm_angles = classes['rest']
            elif np.argmax(y_pred[:,seq_length-1,:]) == 1:
                lower_arm_angles = classes['hand open']
            elif np.argmax(y_pred[:,seq_length-1,:]) == 2:
                lower_arm_angles = classes['hand closed']
            elif np.argmax(y_pred[:,seq_length-1,:]) == 3:
                lower_arm_angles = classes['finger point']
            sArm.send_joint_angles(upper_arm_angles + lower_arm_angles)
            
        short_counter = 0

sArm.send_joint_angles( [0.0]*27 )
sArm.close()
board.stop()

