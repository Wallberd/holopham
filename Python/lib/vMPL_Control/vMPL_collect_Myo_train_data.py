## vMPL Virtual Environment console must be open 
import sys
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting' )
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' )
from mpl.unity import UnityUdp
from mpl import JointEnum as MplId
import math, time, copy
import scipy.io as sio
import numpy as np
np.set_printoptions( precision = 3 )
#from pythonmite.inputs.IntanRHD2000 import *
from mite.inputs import MyoArmband

myo_arm = ['right','left'][0]
classes = { 'rest': [0.0, 0.2, 0.0, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.2, 0.2, 1.57, 1.1, 0.25, 0.4], 
            'hand open': [0.0, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            'hand closed': [0.0, 0.0, 0.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.2, 0.75, 0.75, 1.0],
            'finger point': [0.0, 0.0, 0.0, 1.0, 0.0, 0.1, 0.1, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.0, 1.5, 1.0, 1.0, 1.2, 0.75, 0.75, 1.0] }
upper_arm_angles = [0.0, 0.0, 0.0, 1.57]
lower_arm_angles = [0.0] * 23
loop_time = 5
emgSamplingRate = 200
savefile = '/home/jtkrall/Documents/onlinetesting/rnn_experiments/offline_multi_dof/data/demo_classification.mat'

### MAIN code ##########################
print( "Initializing myo...", flush=True )
# myo = MyoArmband( mac = 'ff:f5:c9:fc:bc:17' ) 
myo = MyoArmband( mac = 'eb:33:40:96:ce:a5' )
# myo = MyoArmband( com = '/dev/ttyACM0', mac = 'e0:f2:99:e7:60:40' )
# myo = MyoArmband( mac = 'd2:8c:41:0e:05:33' )
myo.run( display = False )
print( "Myo initialized.", flush=True )


## Configure which hands/arms
if myo_arm == 'right': # set up for RIGHT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25000', local_address='//0.0.0.0:25001')
else: # set up for LEFT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25100', local_address='//127.0.0.1:25101')
print('Configured arm...', flush=True)

## Open hand/arm connections
sArm.connect()
print('Opened arm connection...', flush=True)

#board = IntanRHD2000()
#board.run()
#print('Started HD-EMG board', flush=True)

rawMyoData, labels = [], []

for loop in range(3):
    t0 = time.time()
    class_idx = 0
    for key in classes:
        print(key, flush=True)
        
        main_counter, short_counter = 1,1
        sys.stdout.flush()
        
        ## Output arm angles to vMPL via UDP
        lower_arm_angles = classes[key]
        sArm.send_joint_angles(upper_arm_angles + lower_arm_angles)
        time.sleep(1/50) # allows arm time to actually get to new position
        
        t0 = time.time()
        while time.time()-t0 <= loop_time:
            rawMyoData.append( myo.state )
            labels.append( class_idx )
            time.sleep(1/emgSamplingRate)
            
        class_idx += 1

sArm.send_joint_angles( [0.0]*27 )
sArm.close()
myo.stop()

print('Prepping Data... ', flush=True)
rawEMG = np.vstack( rawMyoData )
labels = np.vstack( labels )

print('Saving Data... ', flush=True)
sio.savemat(savefile, dict([('rawEMG', rawEMG),('labels', labels)]))