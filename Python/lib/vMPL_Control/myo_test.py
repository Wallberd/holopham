import sys, time, math, matplotlib
matplotlib.use('QT5Agg')
matplotlib.rcParams['image.cmap'] = 'plasma'
import matplotlib.pyplot as plt

import numpy as np
np.set_printoptions( precision = 3, threshold=np.inf)

 # local/personal packages and functions
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' ) # path to "mite" folder
from mite.inputs import MyoArmband
from mite.filters import TimeDomainFilter


### User Vars ##########################
looptime = 10
emgSamplingRate = 200
stepsize, window = 5, 40


### MAIN code ##########################
print( "Initializing myo...", flush=True )
myo = MyoArmband( com = '/dev/ttyACM0', mac = 'e0:f2:99:e7:60:40' )
myo.run( display = False )
td5 = TimeDomainFilter( eps_zc = 1e-6, eps_ssc = 1e-6 )
print( "Myo initialized.", flush=True )


## show myo output for 10 sec, if desired
myo.view()
time.sleep(10)
myo.hide()


## collect myo data in a timed loop
rawMyoData, featureData, imuData = [],[],[]
main_counter, short_counter = 1,1
t0 = time.time()
while time.time()-t0 <= looptime:   
    rawMyoData.append( myo.state ) 
    if main_counter>=window and short_counter>=stepsize: 
        short_counter = 0
        ### get feature data
        tempdata = np.vstack( rawMyoData[main_counter-window:main_counter] )[:,:8]
        current_feature_vector = td5.filter( tempdata )
        featureData.append( current_feature_vector )   
        imuData.append( rawMyoData[-1][8:])   
        # print(rawMyoData[-1][8:],flush=True)

    main_counter += 1
    short_counter += 1
    time.sleep( 1/emgSamplingRate )


 # stack data for processing/saving etc.
featureData = np.vstack( featureData )
rawMyoData = np.vstack( rawMyoData )
imuData = np.vstack( imuData )

data_rate = main_counter/looptime
print('Data rate:', data_rate, flush=True)
print('Effective sampling rate:', 1/data_rate, flush=True)
print(imuData.shape)
print(featureData.shape)
print(rawMyoData.shape, flush=True)
myo.stop()


