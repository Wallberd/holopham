import sys, time, math, matplotlib, pymouse
matplotlib.use('QT5Agg')
import matplotlib.pyplot as plt
import utils, metrics # must import AFTER line 2: matplotlib.use('QT5Agg')

sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' )

import numpy as np
np.set_printoptions( precision = 2 )
import scipy.signal as sig

from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding, LSTM, Conv2D, Conv1D, Dropout
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler
import rnn_models as rnn
from joseph_functions import dof_decode, dof_encode, lstm_fix_data, decode_datablock, get_dof_accuracy


### Global Vars ##########################
seq_length = 8
batch_size = 6
sub_epochs = 100
epochs = 100
stateful_Enable = False
first_timeout = 4
inner_timeout = 4 # seconds
num_classes = 6
conv = 25
lstm_nodes = 64
vel_threshold = 0.02
axis_range = 1.2
stateful_Enable = False
n_nodes = [64, 96]
model_type = ["BidirLSTM", "tCNN",  "ED-TCN", "TDNN", "LSTM"][0]
majority_filter_length = 5
max_len = seq_length
causal = [False, True][0]


### Function Definitions ##########################
def getMouseXY(): ## returns current x,y mouse position as 1x2 numpy array  
    dimx, dimy = mouse.screen_size()
    x_abs, y_abs = mouse.position()
    x, y = x_abs / (dimx-1), 1-y_abs / (dimy-1)
    return np.array([x, y])


### MAIN code ##########################
mouse = pymouse.PyMouse() # Activate streaming mouse

## Seed loop... to initialize RNNs with this first batch
vel_targets, vel_data, vel_decoded_targets = [], [], []
main_counter = 1
sys.stdout.flush()
t0 = time.time()
lastvel = np.zeros(2)
lastpos = getMouseXY()
plt.figure(1, figsize=(5,5))
while time.time() <= t0 + first_timeout:      
    if main_counter>=seq_length: 
        pos = getMouseXY()  # X,Y mose position
        vel = np.subtract( pos, lastpos )  # deltaX, deltaY  
        vel_encoded = np.hstack( (dof_encode(vel[0], vel_threshold), dof_encode(vel[1], vel_threshold)) ) # encode data, e.g (vx,xy):(-0.1,0.03) --> [0 1 0, 1 0 0], and (-0.1,0.13) --> [0 1 0, 0 0 1]
        vel_data.append( np.hstack((vel,pos)) ) 
        # vel_data.append( vel ) 
        vel_targets.append( vel_encoded ) # add current encoded vel to set of targets              
        vel_decoded = [  dof_decode(vel_encoded[0:3]), dof_decode(vel_encoded[3:6]) ]     
        # vel_decoded_targets.append( vel_decoded )        

        vel_line = np.vstack( (lastvel, vel_decoded) )
        plt.axis([-axis_range, axis_range, -axis_range, axis_range])
        plt.plot( vel_line[:,0], vel_line[:,1], 'b', linewidth=2)     
        plt.scatter( vel_decoded[0], vel_decoded[1], c='b', s=25)
        lastpos = pos
        lastvel = vel_decoded
        plt.pause(0.001)
        plt.cla()
        short_counter = 0

    main_counter +=1
    time.sleep(0.001)
plt.close(1)

 ## Prints data shape and features, shape for verification
print('Stacking Data...', flush=True)
Xdata = np.vstack( vel_data )
ylabels = np.vstack( vel_targets )
featureScaler = RobustScaler()
featureScaler = featureScaler.fit( Xdata )
# labelScaler = MinMaxScaler(feature_range=(-1, 1))
# labelScaler = labelScaler.fit( ylabels )
Xdata = featureScaler.transform( Xdata )
# ylabels = labelScaler.transform( ylabels )
num_features = Xdata.shape[1]


n_train = len(Xdata)//seq_length
X_train_m, Y_train_, M_train = utils.mask_data_new2(Xdata, ylabels, max_len, n_train, mask_value=-1) # format data for TCN, biLSTM, etc
(X_train, y_train) = lstm_fix_data(Xdata, ylabels, seq_length) # format 2d sequence data into 3d use for keras


 ## Set up LSTM model
print('Building model...', model_type, flush=True)
if model_type == "LSTM":
    model = Sequential()
    model.add(LSTM(lstm_nodes, input_shape=(X_train.shape[1], X_train.shape[2]), stateful=stateful_Enable, return_sequences=False))
    model.add( Dense(y_train.shape[1], activation="sigmoid") )
    model.compile( loss='categorical_crossentropy', optimizer = 'adam' )
    model.fit( X_train, y_train, epochs=sub_epochs, batch_size=batch_size, verbose=0 )
if model_type == "BidirLSTM":
    model, param_str = rnn.BidirLSTM(n_nodes[0], num_classes, num_features, causal=causal, return_param_str=True)
    model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 
if model_type == "ED-TCN":
    model, param_str = rnn.ED_TCN(n_nodes, conv, num_classes, num_features, max_len, causal=causal, activation='norm_relu', return_param_str=True) 
    model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 
if model_type == "TDNN":
    model, param_str = rnn.TimeDelayNeuralNetwork(n_nodes, conv, num_classes, num_features, max_len, causal=causal, activation='tanh', return_param_str=True)
    model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 
if model_type == "tCNN":
    model, param_str = rnn.temporal_convs_linear(n_nodes[0], conv, num_classes, num_features, max_len, causal=causal, return_param_str=True)
    model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 
# model.summary()

# print("Inputs: {}".format(model.input_shape))
# print("Outputs: {}".format(model.output_shape))
# print("Actual input: {}".format(X_train.shape))
# print("Actual output: {}".format(y_train.shape))


if model_type=="LSTM":
    y_pred = model.predict(X_train, verbose=0)
    ypred_decoded = decode_datablock( y_pred )
    ytrain_decoded = decode_datablock( y_train )
    accs = get_dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
    print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", Global acc: ", accs[2], flush=True)
else:
    y_pred = model.predict(X_train_m, verbose=0)
    y_pred = utils.unmask(y_pred, M_train)
    ypred_decoded = decode_datablock( np.vstack( y_pred ) )    
    ytrain_decoded = decode_datablock( np.vstack( Y_train_ ) )
    accs = get_dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
    print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", Global acc: ", accs[2], flush=True)
    
# plt.figure(2, figsize=(15,3))
# plt.ion()
# plt.subplot(2,1,1); plt.imshow(ytrain_decoded.T)
# plt.subplot(2,1,2); plt.imshow(ypred_decoded.T)
# plt.show() 
# plt.pause(1)
# plt.close(2)

 ## Epoch loop... to online batch training LSTM plus output prediction
allAccs = []
for k in range(0, epochs):
    ## Inner loop vars and var resets
    vel_targets, vel_data, vel_decoded_targets = [], [], []
    main_counter = 1    
    sys.stdout.flush()
    t0 = time.time()
    lastvel = np.zeros(2)
    lastpos = getMouseXY()
    ct = 0
    plt.figure(1, figsize=(5,5))
    while time.time() <= t0 + inner_timeout:           
        ## grab last 'window' samples and compute td5 features if a step-size has passed
        if main_counter>=seq_length: 
            ct += 1   
            pos = getMouseXY()  # X,Y mose position
            vel = np.subtract( pos, lastpos )  # deltaX, deltaY  
            vel_encoded = np.hstack( (dof_encode(vel[0], vel_threshold), dof_encode(vel[1], vel_threshold)) ) # encode data, e.g (vx,xy):(-0.1,0.03) --> [0 1 0, 1 0 0], and (-0.1,0.13) --> [0 1 0, 0 0 1]
            vel_data.append( np.hstack((vel,pos)) ) 
            # vel_data.append( vel ) 
            vel_targets.append( vel_encoded ) # add current encoded vel to set of targets   
            vel_decoded = [  dof_decode(vel_encoded[0:3]), dof_decode(vel_encoded[3:6]) ]  # for plotting against predictions only
            vel_line = np.vstack( (lastvel, vel_decoded) )
            
            if ct <= seq_length:  # if seq_length not yet long enough, use last target as starting point
                last_pred = vel_decoded
                
            if ct > seq_length:   # if datstream longer than seq_length, start predicting on stream   
                sequence = featureScaler.transform( np.vstack( vel_data[ct-1-seq_length:ct-1] ) )
                # sequence = np.vstack( feats[ct-1-seq_length:ct-1] ) 
                sequence = np.reshape(sequence, (1,sequence.shape[0],sequence.shape[1])) # reshape input pattern to [samples, timesteps, features]
                pred_block = np.squeeze( model.predict(sequence, batch_size=1) )
                pred_hot = pred_block[-1]
                pred_decoded = np.hstack( ( dof_decode(pred_hot[0:3]), dof_decode(pred_hot[3:6]) ) )
               
                pred_line = np.vstack( (last_pred, pred_decoded) )
                plt.axis([-axis_range, axis_range, -axis_range, axis_range])
                plt.plot( pred_line[:,0], pred_line[:,1], 'r--', linewidth=2) # plots prediction alongside target
                plt.scatter( pred_decoded[0], pred_decoded[1], c='r', s=25)
                last_pred = pred_decoded
            

            plt.plot( vel_line[:,0], vel_line[:,1], 'b', linewidth=2) # plots target 
            plt.scatter( vel_decoded[0], vel_decoded[1], c='b', s=25)
            lastpos = pos
            lastvel = vel_decoded
            plt.pause(0.001)
            plt.cla()      

        main_counter += 1
        time.sleep(0.001)
    plt.close(1)  


    Xdata = np.vstack( vel_data )
    ylabels = np.vstack( vel_targets )
    Xdata = featureScaler.transform( Xdata )
    # ylabels = labelScaler.transform( ylabels )

    n_train = len(Xdata)//seq_length
    X_train_m, Y_train_, M_train = utils.mask_data_new2(Xdata, ylabels, max_len, n_train, mask_value=-1) # format data for TCN, biLSTM, etc
    (X_train, y_train) = lstm_fix_data(Xdata, ylabels, seq_length) # format 2d sequence data into 3d shape for keras
    
    if model_type=="LSTM":
        y_pred = model.predict(X_train, verbose=0)
        ypred_decoded = decode_datablock( y_pred )
        ytrain_decoded = decode_datablock( y_train )
        accs = get_dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
        print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", Global acc: ", accs[2], flush=True)
        model.fit( X_train, y_train, epochs=sub_epochs, batch_size=batch_size, verbose=0 )
    else:
        y_pred = model.predict(X_train_m, verbose=0)
        y_pred = utils.unmask(y_pred, M_train)
        ypred_decoded = decode_datablock( np.vstack( y_pred ) )    
        ytrain_decoded = decode_datablock( np.vstack( Y_train_ ) )
        accs = get_dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
        print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", Global acc: ", accs[2], flush=True)
        model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 


    allAccs.append(accs)  
    currentAccs = np.vstack(allAccs)     
    plt.figure(3, figsize=(6,5))
    plt.ion()
    plt.cla()
    if k<9:
        plt.plot( currentAccs[:,0], linewidth=2); plt.plot( currentAccs[:,1], linewidth=2); plt.plot( currentAccs[:,2], linewidth=2)
    else:
        plt.plot( sig.savgol_filter(currentAccs[:,0],9,3), linewidth=2); plt.plot( sig.savgol_filter(currentAccs[:,1],9,3), linewidth=2); plt.plot( sig.savgol_filter(currentAccs[:,2],9,3), linewidth=2)
    plt.legend(('DoF1', 'DoF2', 'Total'), loc=4)
    plt.show() 
    plt.pause(.001)

    # plt.figure(2, figsize=(15,3))
    # plt.ion()
    # plt.subplot(2,1,1); plt.imshow(ytrain_decoded.T)
    # plt.subplot(2,1,2); plt.imshow(ypred_decoded.T)
    # plt.show() 
    # plt.pause(1)
    # plt.close(2)
        

# plt.close()
print( 'Done streaming...', flush=True )
exit()


