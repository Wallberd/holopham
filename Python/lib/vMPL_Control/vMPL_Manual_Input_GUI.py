## vMPL Virtual Environment console must be open 
import sys
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting' )
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' )
from mpl.unity import UnityUdp
from tkinter import *

def fetch(entries):
    arm_angles = [0.0] * 27
    idx = 0
    for entry in entries:
      field = entry[0]
      text  = entry[1].get()
      arm_angles[idx] = float(text)
      idx = idx+1
    print(arm_angles, flush=True)
    sArm.send_joint_angles(arm_angles)

def makeform(root, fields):
   entries = []
   for field in fields:
      row = Frame(root)
      lab = Label(row, width=15, text=field, anchor='w')
      ent = Entry(row)
      row.pack(side=TOP, fill=X, padx=5, pady=5)
      lab.pack(side=LEFT)
      ent.pack(side=RIGHT, expand=YES, fill=X)
      entries.append((field, ent))
   return entries

mpl_arm = ['right','left'][1]
fields = ['SHOULDER_FE', 'SHOULDER_AB_AD', 'HUMERAL_ROT', 'ELBOW', 'WRIST_ROT', 'WRIST_AB_AD', 'WRIST_FE', 'INDEX_AB_AD', 'INDEX_MCP', 'INDEX_PIP', 'INDEX_DIP', 'MIDDLE_AB_AD', 'MIDDLE_MCP', 'MIDDLE_PIP', 'MIDDLE_DIP', 'RING_AB_AD', 'RING_MCP', 'RING_PIP', 'RING_DIP', 'LITTLE_AB_AD', 'LITTLE_MCP', 'LITTLE_PIP', 'LITTLE_DIP', 'THUMB_CMC_AB_AD', 'THUMB_CMC_FE', 'THUMB_MCP', 'THUMB_DIP']

## Configure which hands/arms
if mpl_arm == 'right': # set up for RIGHT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25000', local_address='//0.0.0.0:25001')
else: # set up for LEFT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25100', local_address='//127.0.0.1:25101')
print('Configured arm...', flush=True)

## Open hand/arm connections
sArm.connect()
print('Opened arm connection...', flush=True)

## Open GUI
root = Tk()
ents = makeform(root, fields)
root.bind('<Return>', (lambda event, e=ents: fetch(e)))   
b1 = Button(root, text='Send',
       command=(lambda e=ents: fetch(e)))
b1.pack(side=LEFT, padx=5, pady=5)
b2 = Button(root, text='Quit', command=root.quit)
b2.pack(side=LEFT, padx=5, pady=5)
root.mainloop()
        
sArm.close()
print('Closed arm connection.', flush=True)