import sys, time, math, matplotlib
matplotlib.use('QT5Agg')
import matplotlib.pyplot as plt
import utils, metrics # must import AFTER line 2: matplotlib.use('QT5Agg')

import numpy as np
np.set_printoptions( precision = 4 )
import scipy.signal as sig

from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding, LSTM, Conv2D, Conv1D, Dropout
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler

 # local/personal packages and functions
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting' )
import rnn_models as rnn
from mpl.unity import UnityUdp
from mpl import JointEnum as MplId
from joseph_functions import *
from mite.inputs import MyoArmband
from mite.filters import TimeDomainFilter
from mite.inputs.InertialMeasurementUnits import *
from mite.utils import Quaternion as quat


### User Vars ##########################
stepsize, window = 20, 40
seq_length = 3
batch_size = 6
sub_epochs = 100
epochs = 35
stateful_Enable = False
first_timeout = 10
inner_timeout = 8 # seconds
num_classes = 9
conv = 25
lstm_nodes = 64
vel_threshold = 5
axis_range = 1.2
stateful_Enable = False
n_nodes = [64, 96]
model_type = ["BidirLSTM", "tCNN",  "ED-TCN", "TDNN", "LSTM"][0]
majority_filter_length = 5
max_len = seq_length
causal = [False, True][0]
mav_indeces = np.arange( 0, 40, 5 ) # index of 8 MAV features/ch within TD5 (40dim)
t_step = 0.005 # delay between each iteration, currently 5 ms / 200 Hz (don't want to loop faster than myo collects data) 
Right_arm_Enable = [False, True][1]
imu_Enable = [False, True][1]
numDoFs = 3


### MAIN code ##########################
print( "Start streaming...", flush=True )
# myo = MyoArmband( com = '/dev/ttyACM0', mac = 'ff:f5:c9:fc:bc:17' ) # mac = 'eb:33:40:96:ce:a5' ) 
myo = MyoArmband( com = '/dev/ttyACM0', mac = 'eb:33:40:96:ce:a5' )
# myo = MyoArmband( com = '/dev/ttyACM0', mac = 'd2-8c-41-0e-05-33' )
td5 = TimeDomainFilter( eps_zc = 1e-6, eps_ssc = 1e-6 )
myo.run()
time.sleep(1)


print( "Please hold for calibration...", flush=True )
imu = InertialMeasurementUnits( com = "/dev/ttyACM1", chan = [ 2,3,4 ], srate = 50.0 )
time.sleep( 2 )
imu.set_calibrate()
imu.run( display = False )
# imu.view() # unmask to view IMU outputs
time.sleep( 1 )


## Configure which hands/arms
print( 'Opening arm connection...', flush=True )    
if Right_arm_Enable: # set up for RIGHT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25000', local_address='//0.0.0.0:25001')
else: # set up for LEFT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25100', local_address='//127.0.0.1:25100')
sArm.connect()


### Test block to see if MYO and IMU are working ###############
# myo.view() # turn on to see myo datastream graphically
# imu.view()
# time.sleep(5) # allows myo band time to start streaming
# myo.hide()
# myo.stop()
# imu.hide()
# imu.stop()
# exit()


## Seed loop... to initialize RNNs with this first batch
print( 'Begin initial train session...', flush=True )  
rawMyoData, imuData, featureData, vel_targets, vel_decoded_targets = [], [], [], [], []
main_counter, short_counter = 1,1
sys.stdout.flush()
t0 = time.time()
lastvel = np.zeros(numDoFs)
lastpos = get_pos( get_angles( imu.state ) )*180/math.pi # degrees 
while time.time() <= t0 + first_timeout:     
    rawMyoData.append( myo.state ) 
    if main_counter>=window and short_counter>=stepsize: 
        tempdata = np.vstack( rawMyoData[main_counter-window:main_counter] )[:,:8]
        current_feature_vector = td5.filter( tempdata )
        # current_feature_vector = current_feature_vector[ mav_indeces ]
        featureData.append( current_feature_vector )  
        angles = get_angles( imu.state )     
        pos = get_pos( angles )*180/math.pi # in degrees 
        # print(pos, flush=True ) 
        vMPL_angles = set_vMPL_angles( angles )
        sArm.send_joint_angles(vMPL_angles)    
        vel = np.subtract( pos, lastpos )  # deltaX, deltaY 
        lastpos = pos 
        # print( vel, flush=True )   

        vel_encoded = np.hstack( (dof_encode(vel[0], vel_threshold), dof_encode(vel[1], vel_threshold), dof_encode(vel[2], vel_threshold)) )             
        vel_targets.append( vel ) # add current encoded vel to set of targets                
        # vel_decoded = [  dof_decode(vel_encoded[0:3]), dof_decode(vel_encoded[3:6]) ]        
        # vel_decoded_targets.append( vel_decoded )         
        short_counter = 0

    main_counter +=1
    short_counter += 1
    time.sleep(t_step)
print(main_counter)
# exit()

 ## Prints data shape and features, shape for verification
print('Stacking Data...', flush=True)
Xdata = np.vstack( featureData )
ylabels = np.vstack( vel_targets )
# print(ylabels)
ylabels = encode_datablock(ylabels, vel_threshold)
featureScaler = RobustScaler()
featureScaler = featureScaler.fit( Xdata )
# labelScaler = MinMaxScaler(feature_range=(-1, 1))
# labelScaler = labelScaler.fit( ylabels )
Xdata = featureScaler.transform( Xdata )
# ylabels = labelScaler.transform( ylabels )
num_features = Xdata.shape[1]
# print(ylabels)
# exit()

n_train = len(Xdata)//seq_length
print(Xdata.shape,flush=True)
print(ylabels.shape,flush=True)
X_train_m, Y_train_, M_train = utils.mask_data_new2(Xdata, ylabels, max_len, n_train, mask_value=-1) # format data for TCN, biLSTM, etc
(X_train, y_train) = lstm_fix_data(Xdata, ylabels, seq_length) # format 2d sequence data into 3d use for keras


 ## Set up LSTM model
print('Building model...', model_type, flush=True)
if model_type == "LSTM":
    model = Sequential()
    model.add(LSTM(lstm_nodes, input_shape=(X_train.shape[1], X_train.shape[2]), stateful=stateful_Enable, return_sequences=False))
    model.add( Dense(y_train.shape[1], activation="sigmoid") )
    model.compile( loss='categorical_crossentropy', optimizer = 'adam' )
    model.fit( X_train, y_train, epochs=sub_epochs, batch_size=batch_size, verbose=0 )
if model_type == "BidirLSTM":
    model, param_str = rnn.BidirLSTM(n_nodes[0], num_classes, num_features, causal=causal, return_param_str=True)
    model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 
if model_type == "ED-TCN":
    model, param_str = rnn.ED_TCN(n_nodes, conv, num_classes, num_features, max_len, causal=causal, activation='norm_relu', return_param_str=True) 
    model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 
if model_type == "TDNN":
    model, param_str = rnn.TimeDelayNeuralNetwork(n_nodes, conv, num_classes, num_features, max_len, causal=causal, activation='tanh', return_param_str=True)
    model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 
if model_type == "tCNN":
    model, param_str = rnn.temporal_convs_linear(n_nodes[0], conv, num_classes, num_features, max_len, causal=causal, return_param_str=True)
    model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 
model.summary()


print("Inputs: {}".format(model.input_shape))
print("Outputs: {}".format(model.output_shape))
print("Actual input: {}".format(X_train.shape))
print("Actual output: {}".format(y_train.shape))


if model_type=="LSTM":
    y_pred = model.predict(X_train, verbose=0)
    ypred_decoded = decode_datablock( y_pred )
    ytrain_decoded = decode_datablock( y_train )
    accs = get_dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
    print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", Global acc: ", accs[2], flush=True)
else:
    y_pred = model.predict(X_train_m, verbose=2)
    y_pred = utils.unmask(y_pred, M_train)
    ypred_decoded = decode_datablock( np.vstack( y_pred ) )    
    ytrain_decoded = decode_datablock( np.vstack( Y_train_ ) )
    accs = get_dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
    print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", Global acc: ", accs[2], flush=True)
    
plt.figure(2, figsize=(15,3))
plt.ion()
plt.subplot(2,1,1); plt.imshow(ytrain_decoded.T)
plt.subplot(2,1,2); plt.imshow(ypred_decoded.T)
plt.show() 
plt.pause(3)
# exit()
plt.close(2)

 ## Epoch loop... to online batch training LSTM plus output prediction
allAccs = []
print('Begin testing sessions...')  
for k in range(0, epochs):
    rawMyoData, imuData, featureData, vel_targets, vel_decoded_targets = [], [], [], [], []
    main_counter, short_counter = 1,1
    sys.stdout.flush()
    t0 = time.time()
    lastvel = np.zeros(numDoFs)
    lastpos = get_pos( get_angles( imu.state ) )*180/math.pi # degrees 
    while time.time() <= t0 + first_timeout:     
        rawMyoData.append( myo.state ) 
        if main_counter>=window and short_counter>=stepsize: 
            tempdata = np.vstack( rawMyoData[main_counter-window:main_counter] )[:,:8]
            current_feature_vector = td5.filter( tempdata )
            # current_feature_vector = current_feature_vector[ mav_indeces ]
            featureData.append( current_feature_vector )  
            angles = get_angles( imu.state )     
            pos = get_pos( angles )*180/math.pi # in degrees 
            # print(pos, flush=True ) 
            vMPL_angles = set_vMPL_angles( angles )
            sArm.send_joint_angles(vMPL_angles)    
            vel = np.subtract( pos, lastpos )  # deltaX, deltaY 
            lastpos = pos 
            # print( vel, flush=True )   

            vel_encoded = np.hstack( (dof_encode(vel[0], vel_threshold), dof_encode(vel[1], vel_threshold), dof_encode(vel[2], vel_threshold)) )             
            vel_targets.append( vel ) # add current encoded vel to set of targets                
            # vel_decoded = [  dof_decode(vel_encoded[0:3]), dof_decode(vel_encoded[3:6]) ]        
            # vel_decoded_targets.append( vel_decoded )
            short_counter = 0  

        main_counter += 1
        short_counter += 1
        time.sleep(t_step)


    Xdata = np.vstack( featureData )
    ylabels = np.vstack( vel_targets )
    Xdata = featureScaler.transform( Xdata )
    ylabels = encode_datablock(ylabels, vel_threshold)

    n_train = len(Xdata)//seq_length
    print(Xdata.shape,flush=True)
    print(ylabels.shape,flush=True)
    X_train_m, Y_train_, M_train = utils.mask_data_new2(Xdata, ylabels, max_len, n_train, mask_value=-1) # format data for TCN, biLSTM, etc
    (X_train, y_train) = lstm_fix_data(Xdata, ylabels, seq_length) # format 2d sequence data into 3d shape for keras

    if model_type=="LSTM":
        y_pred = model.predict(X_train, verbose=0)
        ypred_decoded = decode_datablock( y_pred )
        ytrain_decoded = decode_datablock( y_train )
        accs = get_dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
        print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", DoF3 acc: ", accs[2], flush=True)
        model.fit( X_train, y_train, epochs=sub_epochs, batch_size=batch_size, verbose=0 )
    else:
        y_pred = model.predict(X_train_m, verbose=0)
        y_pred = utils.unmask(y_pred, M_train)
        ypred_decoded = decode_datablock( np.vstack( y_pred ) )    
        ytrain_decoded = decode_datablock( np.vstack( Y_train_ ) )
        accs = get_dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
        print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", DoF3 acc: ", accs[2], flush=True)
        model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 

     ## plot accuracies
    allAccs.append(accs)  
    currentAccs = np.vstack(allAccs)     
    plt.figure(3, figsize=(6,5))
    plt.ion()
    plt.cla()
    if k<9:
        plt.plot( currentAccs[:,0], linewidth=2); plt.plot( currentAccs[:,1], linewidth=2); plt.plot( currentAccs[:,2], linewidth=2)
    else:
        plt.plot( sig.savgol_filter(currentAccs[:,0],9,3), linewidth=2); plt.plot( sig.savgol_filter(currentAccs[:,1],9,3), linewidth=2); plt.plot( sig.savgol_filter(currentAccs[:,2],9,3), linewidth=2)
    plt.legend(('DoF1', 'DoF2', 'DoF3'), loc=4)
    plt.show() 
    plt.pause(.001)

     ## plot classification output
    plt.figure(2, figsize=(15,4))
    plt.ion()
    plt.subplot(2,1,1); plt.imshow(ytrain_decoded.T)
    plt.subplot(2,1,2); plt.imshow(ypred_decoded.T)
    plt.show() 
    plt.pause(3)
    plt.close(2)
        

# plt.close()
myo.stop()
sArm.send_joint_angles([0.0] * 27)
sArm.close()
imu.stop()
print( 'Done streaming...', flush=True )