import sys, time, math

 # local/personal packages and functions
sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting' )
# sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting/vMPL_Control' )
from mite.inputs.CyberGlove import *
import numpy as np

def calibrateGlove(cyber):
    minGlove = np.zeros(27)
    maxGlove = np.zeros(27)
    
    print('extend Fingers\n')
    time.sleep(2)
    state = cyber.state()
    val = mudFormat(state)
    minGlove[ [9,10,11,13,14,15,17,18,19,21,22,23] ] = val[ [9,10,11,13,14,15,17,18,19,21,22,23] ]
    
    print('flex Fingers\n')
    time.sleep(2)
    val = mudFormat(state())
    maxGlove[ [9,10,11,13,14,15,17,18,19,21,22,23] ] = val[ [9,10,11,13,14,15,17,18,19,21,22,23] ]
    
#        print('extend Finger AbAds and Thumb FE\n')
#        time.sleep(2)
#        val = obj.mudFormat(obj.getRawData()');
#        obj.maxGlove([16 20]) = val([16 20]);
#        obj.minGlove([8 25]) = val([8 25]);
#        
#        print('flex Finger AbAds and Thumb FE\n')
#        time.sleep(2)
#        val = obj.mudFormat(obj.getRawData()');
#        obj.minGlove([16 20]) = val([16 20]);
#        obj.maxGlove([8 25]) = val([8 25]);
#        
#        print('extend distal Thumb\n')
#        time.sleep(2)
#        val = obj.mudFormat(obj.getRawData()');
#        obj.minGlove([26 27]) = val([26 27]);
#        
#        print('flex distal Thumb\n')
#        time.sleep(2)
#        val = obj.mudFormat(obj.getRawData()');
#        obj.maxGlove([26 27]) = val([26 27]);
#        
#        print('extend Thumb AbAd\n')
#        time.sleep(2)
#        val = obj.mudFormat(obj.getRawData()');
#        obj.minGlove(24) = val(24);
#        
#        print('flex Thumb AbAd\n')
#        time.sleep(2)
#        val = obj.mudFormat(obj.getRawData()');
#        obj.maxGlove(24) = val(24);
    
    print('extend Wrist Flexor\n')
    time.sleep(2)
    val = mudFormat(cyber.state())
    minGlove[7] = val[7]
    
    print('flex Wrist Flexor\n')
    time.sleep(2)
    val = mudFormat(cyber.state())
    maxGlove[7] = val[7]
    
    print('extend Wrist Deviator\n')
    time.sleep(2)
    val = mudFormat(cyber.state())
    minGlove[6] = val[6]
    
    print('flex Wrist Deviator\n')
    time.sleep(2)
    val = mudFormat(cyber.state())
    maxGlove[6] = val[6]
    
    return minGlove, maxGlove

def getData(rawData):
    degreesPerCount = 0.5 # ref CyberGlove Manual
    
    anglesDegrees = rawData * degreesPerCount
    
    anglesRadians = anglesDegrees * math.pi / 180
    
    return anglesDegrees, anglesRadians

def mudFormat(raw):
    val = np.zeros(27)
    val[ [6,7] ] = raw[ [22,21] ]
    val[ [8,9,10,11] ] = raw[ [11,5,6,7] ]      # index finger
    val[ [13,14,15] ] = raw[ [8,9,10] ]      # middle finger
    val[ [16,17,18,19] ] = raw[ [15,12,13,14] ]  # ring finger
    val[ [20,21,22,23] ] = raw[ [19,16,17,18] ]  # little finger
    val[ [24,25,26,27] ] = raw[ [1,4,2,3] ]      # thumb
    
    return val

def adjustForMPL(val, minGlove, maxGlove):
    maxLim =   [170,  -15,  85, 135,  90,  45,  60,   0,  90, 100, 80, 0, 90, 100, 80, 20, 90, 100, 80, 20,  90, 100, 80, 135, 80, 80,  60] * math.pi/180
    minLim =   [-35, -110, -35,   5, -90, -15, -60, -20, -30,   0,  0, -20, -30,   0,  0,  0, -30,   0,  0,  0, -30,   0,  0,   0,  0,  0, -20] * math.pi/180
    q = minLim + (maxLim - minLim) * ( (val - minGlove) / (maxGlove - minGlove) )
    
    q[20] = ( q[16] + q[20] ) / 2      # unified Ring/Little AbAd
    q[16] = q[20]                      # /
    q[25] = q[25] + 0.9 * q[24] - 0.2  # adjust for Thumb FE
#    q(1:5) = q_arm(1:5)                # upper arm Microstrain
    q[5] = q[5] * 1.2                  # increase scale for Wrist Rot
    
 #   q = max(obj.minLim, min(obj.maxLim,q));
    
    return q


print( "Initializing CyberGlove...", flush=True )
cyber = CyberGlove( name = 'CyberGlove', com = '/dev/ttyUSB0', baud = 115200, srate = 100.0 )
cyber.run( display = False )
print( "CyberGlove initialized.", flush=True )
time.sleep(0.5)

#minGlove, maxGlove = calibrateGlove(cyber)

cyber_state2 = np.array(cyber.state)
while 1:
#    cyber_state = cyber.state
#    anglesDegrees, anglesRadians = getData(cyber_state)
##    print( anglesRadians, flush = True )
##    hand = 0.5 * ( cyber_state[4] + cyber_state[8] )
#    cyber_angles = [anglesRadians[20], anglesRadians[21], anglesRadians[4], anglesRadians[7]]
#    print( cyber_angles, flush = True )
#    time.sleep(1)

#   cyber_state = cyber.state
#   anglesDegrees, anglesRadians = getData(cyber_state)
#   val = mudFormat(anglesRadians)
#   final = adjustForMPL(val, minGlove, maxGlove)
#   print(final)
    
    cyber_state1 = cyber.state
    diff = cyber_state1 - cyber_state2
    max_diff =np.argmax(np.absolute(diff))
    #min_diff = np.argmin(diff)
    print(max_diff)
    time.sleep(1)
    






