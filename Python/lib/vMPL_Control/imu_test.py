#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  9 15:05:14 2018

@author: jtkrall
"""

import sys
# sys.path.insert( 0, '/home/jtkrall/Documents/onlinetesting' )
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' )
import math, time

from mite.inputs.InertialMeasurementUnits import *
from mite.utils import Quaternion as quat

import numpy as np
from numpy.linalg import inv

import threespace

## Enables printing to console during for-loops
def my_print(text):
    sys.stdout.write(str(text)+"\n")
    sys.stdout.flush()

## Set up IMU interface
print( "Creating IMU interface..." )
imu = InertialMeasurementUnits( com = "/dev/ttyACM2", chan = [ 2,4 ], srate = 50.0 )

print( "Please hold for calibration..." )
time.sleep( 2 )
imu.set_calibrate()

print( "Start streaming..." )
imu.run( display = False )
imu.view() # unmask to view IMU outputs
time.sleep( 1 )

'''Finds euler angles of single IMU'''
#while 1:
#    state = imu.state
#    euler_angles = quat.quaternion_to_euler_angle(state)
#    print(euler_angles)
#    time.sleep( 1 )


'''Finds euler angle difference between two IMUs'''
# state = imu.state
# num_quat = int( len( state ) / 4 )
# init_angles = []
# for q in range( 1, num_quat ):
#     idx1 = q * 4 - 4
#     idx2 = q * 4
#     q1 = state[ idx1:idx1+4 ]
#     q2 = state[ idx2:idx2+4 ]
#     euler_angles = quat.quaternion_to_euler_angle( quat.relative( q1, q2 ) )
#     init_angles.append(euler_angles)
# while 1: #i <= 300:
#     state = imu.state
#     num_quat = int( len( state ) / 4 )
#     angles = []
#     for q in range( 1, num_quat ):
#         idx1 = q * 4 - 4
#         idx2 = q * 4
#         q1 = state[ idx1:idx1+4 ]
#         q2 = state[ idx2:idx2+4 ]
#         euler_angles = quat.quaternion_to_euler_angle( quat.relative( q1, q2 ) )
#         angles.append(euler_angles)
#     print(euler_angles, flush=True)
#     time.sleep( 1 )
#     print('\n', 'wrist flex: ', -angles[1][0]+init_angles[1][0], flush=True)
#     print('wrist adduct: ', -angles[1][2]+init_angles[1][2], flush=True)
#     print('hand flex: ', -angles[2][0]+init_angles[2][0], flush=True)
#     print('wrist rotate: ', angles[0][1]-init_angles[0][1], flush=True)
#     time.sleep( 2 )
    
'''Finds the rotation between the initial state and current state of a single IMU using quaternion multiplication'''
#i = 0
#ref_state = imu.state
#center = quat.inverse( ref_state )
#while 1:
#    state = imu.state
#    new_orientation = quat.multiply( center, state)
#    euler_angles = quat.quaternion_to_euler_angle( new_orientation )
#    print(euler_angles)
#    time.sleep( 1 )

'''Finds the rotation between two IMUs using quaternion multiplication'''
#ref_state = imu.state
#q1_ref = ref_state[ 0:4 ]
#q2_ref = ref_state[ 4:8 ]
#while 1:
#    state = imu.state
#    q1 = state[ 0:4 ]
#    q2 = state[ 4:8 ]
#    q22 = quat.multiply(quat.inverse(q2_ref), q2)
#    q122 = quat.multiply(q22, q1)
#    q122 = quat.multiply(q122, quat.inverse(q22))
#    euler_angles = quat.quaternion_to_euler_angle( q122 )
#    print(q1)
#    print(q122)
#    time.sleep( 1 )


'''Finds the swing-twist decompesition between the initial state and current state of a single IMU'''
#i = 0
#ref_state = imu.state
#center = quat.inverse( ref_state )
#while 1:
#    state = imu.state
#    new_orientation = quat.multiply( center, state)
#    euler_angles = quat.quaternion_to_euler_angle( new_orientation )
#    swing, twist = quat.swing_twist( new_orientation )
#    st = quat.multiply( swing, twist)
#    st_euler_angles = quat.quaternion_to_euler_angle( st )
#    print(euler_angles)
#    print(st_euler_angles)
#    print("''''")
#    time.sleep( 1 )


'''Finds the rotation matrix between the initial state and current state of a single IMU'''
#i = 0
#ref_state = imu.state
#r1 = quat.to_matrix( ref_state )
#while 1:
#    state = imu.state
#    r2 = quat.to_matrix( state )
#    r12 = np.matmul( np.matrix.transpose( r1 ), r2 )
#    quat_angles = quat.from_matrix( r12 )
#    euler_angles = quat.quaternion_to_euler_angle( quat_angles )
#    print(euler_angles, flush = True )
#    time.sleep( 2 )

'''Finds the rotation matrix between two IMUs'''
#i = 0
#while 1:
#    state = imu.state
#    num_quat = int( len( state ) / 4 )
#    angles = []
#    for q in range( 1, num_quat ):
#        idx1 = q * 4 - 4
#        idx2 = q * 4
#        q1 = state[ idx1:idx1+4 ]
#        q2 = state[ idx2:idx2+4 ]
#        r1 = quat.to_matrix( q1 )
#        r2 = quat.to_matrix( q2 )
#        r12 = np.matmul(np.matrix.transpose(r1),r2)
#        quat_angles = quat.from_matrix(r12)
#        euler_angles = quat.quaternion_to_euler_angle( quat_angles )
#    print(euler_angles, flush = True )
#    time.sleep( .5 )

'''Finds shoulder angles via rotation matrices'''
#i = 0
#ref_state = imu.state
#r1_ref = quat.to_matrix( ref_state[ 0:4 ] )
#r2_ref = quat.to_matrix( ref_state[ 4:8 ] )
#r12_ref = np.matmul( np.matrix.transpose( r1_ref ), r2_ref )
#while 1:
#    state = imu.state
#    num_quat = int( len( state ) / 4 )
#    angles = []
#    for q in range( 1, num_quat ):
#        idx1 = q * 4 - 4
#        idx2 = q * 4
#        q1 = state[ idx1:idx1+4 ]
#        q2 = state[ idx2:idx2+4 ]
#        r1 = quat.to_matrix( q1 )
#        r2 = quat.to_matrix( q2 )
#        r12 = np.matmul( np.matrix.transpose( r1 ), r2 )
#        r13 = np.matmul( np.matrix.transpose( r12_ref ), r12 )
#        quat_angles = quat.from_matrix( r13 )
#        euler_angles = quat.quaternion_to_euler_angle( quat_angles )
#    print(quat_angles, flush = True )
#    time.sleep( .5 )

# '''3-Space calculation of 3-DoF joint angle'''
# while 1:
#    state = imu.state
#    q1 = state[ 0:4 ]
#    q2 = state[ 4:8 ]
   
#    v_forward0 = [0, -1, 0]
#    v_down0 = [1, 0, 0]
   
#    v_forward1 = [0, -1, 0]
#    v_down1 = [1, 0, 0]

#    forward0 = threespace.quaternionVectorMultiplication(q1, v_forward0)
#    down0 = threespace.quaternionVectorMultiplication(q1, v_down0)

#    forward1 = threespace.quaternionVectorMultiplication(q2, v_forward1)
#    down1 = threespace.quaternionVectorMultiplication(q2, v_down1)
   
#    pitch, yaw, roll = threespace.calculatePitchYawRoll(forward0, down0, forward1, down1)
#    print(pitch, yaw, roll, flush = True )
#    time.sleep( .5 )

'''Axis-Angle'''
#while 1:
#    q = imu.state
#    
#    angle = 2*math.acos( q[0] )
#    
#    axis_scale = math.sqrt(1.0 - q[0]*q[0])
#    axis_x = q[1]/axis_scale
#    axis_y = q[2]/axis_scale
#    axis_z = q[3]/axis_scale
#    
#    r= math.sqrt( axis_x*axis_x + axis_y*axis_y + axis_z*axis_z)
#    
#    theta = math.acos( axis_z/r )
#    psi = math.atan2( axis_y, axis_x )
#    
#    print(q, flush = True)
##    print(axis_x, axis_y, axis_z, angle, flush = True )
##    print(theta, psi, angle, flush = True )
#    time.sleep( 2 )
    






