import sys, time, math, matplotlib
matplotlib.use('QT5Agg')
import matplotlib.pyplot as plt
import utils, metrics # must import AFTER line 2: matplotlib.use('QT5Agg')

import numpy as np
np.set_printoptions( precision = 2, threshold=np.inf)
import scipy.signal as sig

from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding, LSTM, Conv2D, Conv1D, Dropout
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler

 # local/personal packages and functions
sys.path.insert( 0, '/home/jlbettha/Documents/onlinetesting' )
import rnn_models as rnn
from mpl.unity import UnityUdp
from mpl import JointEnum as MplId
from joseph_functions import *
from mite.inputs import MyoArmband
from mite.filters import TimeDomainFilter
from mite.inputs.InertialMeasurementUnits import *
from mite.utils import Quaternion as quat


### User Vars ##########################
subject_name = "A1"
stepsize, window = 5, 35
seq_length = 8
max_len = seq_length
batch_size = 6
sub_epochs = 50
epochs = 60
explore_timer = 20 
first_timeout = 15
inner_timeout = 15 # seconds
numDoFs = 3
majority_filter_length = 5
moving_avg_length = 3
tolerance = 0.25 # -/+ on either side of rest position

emgSamplingRate = 200
moving_avg_length = 3
causal = [False, True][0]
stateful_Enable = [False, True][0]
Right_arm_Enable = [False, True][1]
imu_Enable = [False, True][1]
num_classes = 3 * numDoFs
MAV_only = [False, True][1]
mav_indeces = np.arange( 0, 40, 5 ) # index of 8 MAV features/ch within TD5 (40dim)
conv = 25
lstm_nodes = 64
n_nodes = [64, 96]
model_type = ["BLSTM", "TCNN",  "ED-TCN", "TDNN"][0]
rest_timer_cutoff = [2,5]

def init_myo():
    # myo = MyoArmband( com = '/dev/ttyACM0', mac = 'ff:f5:c9:fc:bc:17' ) # mac = 'eb:33:40:96:ce:a5' ) 
    # myo = MyoArmband( com = '/dev/ttyACM0', mac = 'eb:33:40:96:ce:a5' )
    # myo = MyoArmband( com = '/dev/ttyACM0', mac = 'd2:8c:41:0e:05:33' )
    myo = MyoArmband( com = '/dev/ttyACM0', mac = 'e0:f2:99:e7:60:40' )
    td5 = TimeDomainFilter( eps_zc = 1e-6, eps_ssc = 1e-6 )
    myo.run()
    return myo, td5

def init_imu():    
    imu = InertialMeasurementUnits( com = "/dev/ttyACM1", chan = [ 2,3,4 ], srate = 50.0 )
    print( "Please hold 2 seconds for calibration...", flush=True )
    time.sleep( 2 )
    imu.set_calibrate()
    imu.run( display = False )
    # imu.view() # unmask to view IMU outputs
    return imu

def exploreLoop():
    pos_data, smooth_pos_data = [],[]
    disp_flag = 1
    main_counter, short_counter = 1,1
    data_counter = 0
    sys.stdout.flush()
    t0 = time.time()
    pos_data.append( get_pos( get_angles( imu.state ) )*180/math.pi ) # degrees 
    smooth_pos_data.append( pos_data[0] )
    print("REST HAND AND WRIST", flush=True)
    while time.time() <= t0 + explore_timer:     
        if main_counter>=window and short_counter>=stepsize: 
            data_counter += 1
            short_counter = 0
            if time.time() - t0 > rest_timer_cutoff[1] and disp_flag==1:
                print( 'FULLY EXPLORE RANGE OF HAND/WRIST MOTION', flush=True ) 
                disp_flag=0

            ### get position data
            angles = get_angles( imu.state )
            vMPL_angles = set_vMPL_angles( angles )
            sArm.send_joint_angles(vMPL_angles)   
            pos = get_pos( angles )*180/math.pi     # in degrees 
            pos_data.append( pos ) 
            # mvg_avg = np.mean( np.vstack( pos_data[-moving_avg_length:] ),axis=0)  
            # smooth_pos_data.append( mvg_avg )   
            smooth_pos_data.append( pos ) 
            
        main_counter +=1
        short_counter += 1
        time.sleep(1/emgSamplingRate)

    time_passed = time.time()-t0
    smooth_positions = np.vstack(smooth_pos_data)
    
    data_rate = data_counter//time_passed # per second
    print(data_rate, flush=True )
    print(smooth_positions.shape, flush=True )
    posMax = np.max(smooth_positions,0)
    posMin = np.min(smooth_positions,0)
    print(posMax, flush=True )
    print(posMin, flush=True )
    posRest = np.mean(smooth_positions[int(rest_timer_cutoff[0]*data_rate):int(rest_timer_cutoff[1]*data_rate),:],0)    
    print(posRest, flush=True )
    
    return posMin, posMax, posRest

def dataLoop(looptime):
    rawMyoData, imuData, featureData = [], [], []
    pos_data, smooth_pos_data = [],[]
    main_counter, short_counter = 1,1
    data_counter = 0
    sys.stdout.flush()
    t0 = time.time()
    lastvel = np.zeros(numDoFs)
    pos_data.append( get_pos( get_angles( imu.state ) )*180/math.pi ) # degrees 
    smooth_pos_data.append( pos_data[0] )
    while time.time() <= t0 + looptime:     
        rawMyoData.append( myo.state ) 
        if main_counter>=window and short_counter>=stepsize: 
            data_counter += 1
            short_counter = 0

            ### get position and velocity data
            angles = get_angles( imu.state )        
            vMPL_angles = set_vMPL_angles( angles ) ### output joints to vMPL
            sArm.send_joint_angles(vMPL_angles)     ### output joints to vMPL
            pos = get_pos( angles )*180/math.pi     # in degrees 
            pos_data.append( pos ) 
            # mvg_avg = np.mean( np.vstack( pos_data[-moving_avg_length:] ),axis=0)  
            # smooth_pos_data.append( mvg_avg )   
            smooth_pos_data.append( pos )      
            
            ### get feature data
            tempdata = np.vstack( rawMyoData[main_counter-window:main_counter] )[:,:8]
            current_feature_vector = td5.filter( tempdata )
            if MAV_only:
                current_feature_vector = current_feature_vector[ mav_indeces ] # unmask this line to get only MAV features
            featureData.append( current_feature_vector )          
            
        main_counter +=1
        short_counter += 1
        time.sleep(1/emgSamplingRate)
        # time_passed = time.time()-t0
        # print("time: ", time_passed)
        # print("avg EMG freq: ", main_counter/time_passed)
        # print("data/sec: ", data_counter/time_passed)
    return featureData, smooth_pos_data


### MAIN code ##########################
print( "Initializing myo...", flush=True )
myo, td5 = init_myo()
print( "Myo initialized.", flush=True )

print( "Initializing IMUs...", flush=True )
imu = init_imu()
print( "IMUs initialized and calibrated.", flush=True )
time.sleep(0.5)


## Configure which hands/arms  
if Right_arm_Enable: # set up for RIGHT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25000', local_address='//0.0.0.0:25001')
else: # set up for LEFT arm    
    sArm = UnityUdp(remote_address='//127.0.0.1:25100', local_address='//127.0.0.1:25100')
sArm.connect()
print( 'vMPL arm connected.', flush=True )
sArm.send_joint_angles([0.0] * 27)  # sets arm to init position
time.sleep(1)

## Explore Loop - get positions for DoF mins, maxes, and rest
posMin, posMax, posRest = exploreLoop()
# exit()

## Seed loop... to initialize RNNs with this first batch
print( 'Begin initial train session...', flush=True )  
featureData, smooth_pos_data = dataLoop(first_timeout)


 ## Prints data shape and features, shape for verification
print('Prepping Data...', flush=True)
Xdata = np.vstack( featureData )
ylabels = np.vstack( smooth_pos_data )
ylabels = encode_datablock_pos_classify(ylabels, posMin, posMax, posRest, tolerance)
featureScaler = RobustScaler()
featureScaler = featureScaler.fit( Xdata )
Xdata = featureScaler.transform( Xdata )
num_features = Xdata.shape[1]
n_train = len(Xdata)//seq_length
print(Xdata.shape,flush=True)
print(ylabels.shape,flush=True)
X_train_m, Y_train_, M_train = utils.mask_data_new2(Xdata, ylabels, max_len, n_train, mask_value=-1) # format data for TCN, biLSTM, etc
(X_train, y_train) = lstm_fix_data(Xdata, ylabels, seq_length) # format 2d sequence data into 3d use for keras


 ## Set up LSTM model
print('Building model...', model_type, flush=True)
if model_type == "BLSTM":
    model, param_str = rnn.BidirLSTM(n_nodes[0], num_classes, num_features, causal=causal, return_param_str=True) 
if model_type == "ED-TCN":
    model, param_str = rnn.ED_TCN(n_nodes, conv, num_classes, num_features, max_len, causal=causal, activation='norm_relu', return_param_str=True) 
if model_type == "TDNN":
    model, param_str = rnn.TimeDelayNeuralNetwork(n_nodes, conv, num_classes, num_features, max_len, causal=causal, activation='tanh', return_param_str=True)
if model_type == "TCNN":
    model, param_str = rnn.temporal_convs_linear(n_nodes[0], conv, num_classes, num_features, max_len, causal=causal, return_param_str=True)

model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 

# model.summary()
# print("Inputs: {}".format(model.input_shape))
# print("Outputs: {}".format(model.output_shape))
# print("Actual input: {}".format(X_train.shape))
# print("Actual output: {}".format(y_train.shape))

 # predict code-- this first instance just predicts on the training data, not recorded
if model_type=="LSTM":
    y_pred = model.predict(X_train, verbose=0)
    ypred_decoded = decode_datablock( y_pred )
    ytrain_decoded = decode_datablock( y_train )
    accs = get_3dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
    print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", DoF3 acc: ", accs[2], flush=True)
else:
    y_pred = model.predict(X_train_m, verbose=2)
    y_pred = utils.unmask(y_pred, M_train)
    ypred_decoded = decode_datablock( np.vstack( y_pred ) )    
    ytrain_decoded = decode_datablock( np.vstack( Y_train_ ) )
    accs = get_3dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
    print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", DoF3 acc: ", accs[2], flush=True)
    
plt.figure(2, figsize=(15,3)); plt.ion()
plt.subplot(2,1,1); plt.imshow(ytrain_decoded.T)
plt.subplot(2,1,2); plt.imshow(ypred_decoded.T)
plt.show(); plt.pause(1.5); plt.close(2)


 ## Epoch loop... to online batch training LSTM plus output prediction
allAccs = []
print('Begin testing sessions...')  
for k in range(0, epochs):
      ## data gathering function
    featureData, smooth_pos_data = dataLoop(inner_timeout)

    Xdata = np.vstack( featureData )
    Xdata = featureScaler.transform( Xdata )
    ylabels = np.vstack( smooth_pos_data )
    ylabels = encode_datablock_pos_classify(ylabels, posMin, posMax, posRest, tolerance)

    n_train = len(Xdata)//seq_length
    print(Xdata.shape,flush=True)
    # print(ylabels.shape,flush=True)
    X_train_m, Y_train_, M_train = utils.mask_data_new2(Xdata, ylabels, max_len, n_train, mask_value=-1) # format data for TCN, biLSTM, etc
    (X_train, y_train) = lstm_fix_data(Xdata, ylabels, seq_length) # format 2d sequence data into 3d shape for keras

    # predict code-- predict on test data, recorded to file
    if model_type=="LSTM":
        y_pred = model.predict(X_train, verbose=0)
        ypred_decoded = decode_datablock( y_pred )
        ytrain_decoded = decode_datablock( y_train )
        accs = get_3dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
        print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", DoF3 acc: ", accs[2], flush=True)
        model.fit( X_train, y_train, epochs=sub_epochs, batch_size=batch_size, verbose=0 )
    else:
        y_pred = model.predict(X_train_m, verbose=0)
        y_pred = utils.unmask(y_pred, M_train)
        ypred_decoded = decode_datablock( np.vstack( y_pred ) )    
        ytrain_decoded = decode_datablock( np.vstack( Y_train_ ) )
        accs = get_3dof_accuracy(ytrain_decoded, ypred_decoded, majority_filter_length)
        print("DoF1 acc: ", accs[0], ", DoF2 acc: ", accs[1], ", DoF3 acc: ", accs[2], flush=True)
        model.fit(X_train_m, Y_train_, epochs=sub_epochs, batch_size=batch_size, verbose=0, sample_weight=M_train[:,:,0]) 

     ## plot accuracies
    allAccs.append(accs)  
    currentAccs = np.vstack(allAccs)     
    plt.figure(3, figsize=(6,5)); plt.ion(); plt.cla()
    if k<9:
        plt.plot( currentAccs[:,0], linewidth=2); plt.plot( currentAccs[:,1], linewidth=2); plt.plot( currentAccs[:,2], linewidth=2)
    else:
        plt.plot( sig.savgol_filter(currentAccs[:,0],9,3), linewidth=2); plt.plot( sig.savgol_filter(currentAccs[:,1],9,3), linewidth=2); plt.plot( sig.savgol_filter(currentAccs[:,2],9,3), linewidth=2)
    plt.legend(('DoF1', 'DoF2', 'DoF3'), loc=4);  plt.show(); plt.pause(.001)

     ## plot classification output
    plt.figure(2, figsize=(15,4)); plt.ion(); 
    plt.subplot(2,1,1); plt.imshow(ytrain_decoded.T)
    plt.subplot(2,1,2); plt.imshow(ypred_decoded.T)
    plt.show(); plt.pause(1.5); plt.close(2)
        

 ## Program shutdown protocol
# plt.close()
myo.stop()
sArm.send_joint_angles([0.0] * 27)
sArm.close()
imu.stop()
print( 'Done streaming...', flush=True )