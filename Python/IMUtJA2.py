# Modified by Wally Niu

#TODO: clean up imports
import sys

sys.path.insert(0,'lib/')
#sys.path.insert(0,'../../vMPL_Control')


import numpy as np
from transforms3d import quaternions as quat
from transforms3d import euler as eul
from mpl import JointEnum as MplId

import math, time, random#, pylab
# import matplotlib.pyplot as plt

""" IMU to Joint Angles
"""
class IMUtJA2:
    def __init__(self, handedness, imu):
        """ Constructor """
        #Calibration Angles
        self.q23_initial = [0,0,0,0];
        self.q12_initial = [0,0,0,0];
        self.q1_initial  = [0,0,0,0];
        self.a1_90       = [0,0,0,0];
        # Angles current timestep
        self.e1 = [0,0,0];
        self.e2 = [0,0,0];
        self.e3 = [0,0,0];
        if handedness == 'left':
            self.arm_mult = -1
        else:
            self.arm_mult = 1
        self.imu = imu
    def calibrate(self):
        #-----------------------------------------------
        # Rest Position
        print("Hold rest position")
        time.sleep(2)
        print("don't move!")
        time.sleep(2)
        state = self.imu.state()
        q1 = state[8:12]
        q2 = state[4:8]
        q3 = state[0:4]
        q12 = quat.qmult(quat.qinverse(q1),q2)
        q23 = quat.qmult(quat.qinverse(q2),q3)

        self.q1_initial = q1
        self.q12_initial = q12
        self.q23_initial = q23
        print('next position.........')
        #-----------------------------------------------
        # S Flex 90
        print("Shoulder Flex 90deg")
        time.sleep(2)
        print("don't move!")
        '''
        e1_sum = [0,0,0]
        i = 0;
        exp_time = 1;
        t0 = time.time()
        while time.time() - t0 < exp_time:
            state = self.imu.state()
            q1 = state[8:12]
            e1_sum  = e1_sum + np.asarray(eul.quat2euler(q1))
            i = i+1
        self.e1_p1 = e1_sum / i;
        '''
        state = self.imu.state()
        q1 = state[8:12]
        self.a1_90
        q_ideal = eul.euler2quat(0,0,math.pi/2)
        q_act = quat.qmult(quat.qinverse(self.q1_initial),q1)
        self.a1_90 = quat.qmult(quat.qinverse(q_act), q_ideal)
        time.sleep(2)
        print('next position...')
        #-----------------------------------------------
        # S Add 90 (no flex!)
        print('Shoulder Add 90deg')
        time.sleep(2)
        print("don't move!")
        state = self.imu.state()
        q1 = state[8:12]
        q_ideal = eul.euler2quat(math.pi/2,0,0)
        q_act = quat.qmult(quat.qinverse(self.q1_initial),q1)
        q_act_p = quat.qmult(q_act, self.a1_90)
        self.a2_90 = quat.qmult(quat.qinverse(q_act_p), q_ideal)
        time.sleep(2)
        print('next position')
        #-----------------------------------------------
        # E Flex 90
        print('Elbow Flex 90deg')
        time.sleep(2)
        print("don't move!")
        state = self.imu.state()
        q1 = state[8:12]
        q2 = state[4:8]
        q12 = quat.qmult(quat.qinverse(q1),q2)
        #TODO: Check this
        q_ideal = eul.euler2quat(0,math.pi/2,0)
        q_act = quat.qmult(quat.qinverse(self.q12_initial), q12)
        self.a3_90 = quat.qmult(quat.qinverse(q_act),q_ideal)
        time.sleep(2)
        print('next position')

        #-----------------------------------------------
        print('algorithm calibration complete')
    def linear_interpolate(self, qi, q0, q90):
        """
        linear interpolation for a single angle change
        between 0,90
        """
        theta = 0
     #   print('------------------------------')
     #   print('qi', qi)
     #   print('qi-q0', qi-q0)
     #   print('v1', quat.qmult(qi-q0), quat.qinverse(q90-q0))
     #   print('v2', quat.qmult(quat.qinverse(q90-q0), qi-)

        #print (180*quat.to_euler(quat.qmult(qi-q0, quat.qinverse(q90-q0))))

        print('-------------------------------')
        return theta


    def calc(self):
        """
        calculate joint angles based on IMU quaternions
        """
        state = self.imu.state()
        q1 = state[8:12]
        q2 = state[4:8]
        q3 = state[0:4]

        q12 = quat.qmult(quat.qinverse(q1),q2)
        q1_act = quat.qmult(quat.qinverse(self.q1_initial), q1)
        q12_act = quat.qmult(quat.qinverse(self.q12_initial), q12)
        q1_cal = quat.qmult(q1_act,self.a1_90)
        q1_cal = quat.qmult(q1_act, self.a2_90)
        q12_cal = quat.qmult(q12_act, self.a3_90)

        e1 = eul.quat2euler(q1_cal)
        e12 = eul.quat2euler(q12_cal)


        # inital values
        shoulder_flex = e1[2] * 180/math.pi
        shoulder_adduct = e1[0] * 180/math.pi
        humeral_rotate = 0
        elbow_flex = e12[1] * 180/math.pi
        wrist_rotate = 0
        wrist_flex = 0
        wrist_adduct = 0
        hand_flex = 0


        # interpolate shoulder flex
        '''
        q0  = self.q1_initial
        q90 = self.a1_90
        shoulder_flex = self.linear_interpolate(q1, q0, q90);
        '''
        # translate into 27 vMPL arm  angles
        arm_angles = [0.0] * 27
        arm_angles[MplId.SHOULDER_FE] = shoulder_flex
        arm_angles[MplId.SHOULDER_AB_AD] = shoulder_adduct
        arm_angles[MplId.HUMERAL_ROT] = humeral_rotate
        arm_angles[MplId.ELBOW] = elbow_flex
        arm_angles[MplId.WRIST_ROT] = wrist_rotate
        arm_angles[MplId.WRIST_FE] = wrist_flex
        arm_angles[MplId.WRIST_AB_AD] = wrist_adduct
        hand_idx = [MplId.INDEX_MCP, MplId.MIDDLE_MCP, MplId.RING_MCP, MplId.LITTLE_MCP]
        pip_idx = [MplId.INDEX_PIP, MplId.MIDDLE_PIP, MplId.RING_PIP, MplId.LITTLE_PIP, MplId.INDEX_DIP, MplId.MIDDLE_DIP, MplId.RING_DIP, MplId.LITTLE_DIP]
        thumb_idx = [MplId.THUMB_CMC_FE, MplId.THUMB_MCP, MplId.THUMB_DIP]
        arm_angles[MplId.THUMB_CMC_AB_AD] = 0.5 * hand_flex
        for idx in hand_idx:
            arm_angles[idx] = hand_flex
        for idx in pip_idx:
            arm_angles[idx] = 1.0 * hand_flex
        for idx in thumb_idx:
            arm_angles[idx] = 0.6 * hand_flex

#        return arm_angles
        return [shoulder_flex, shoulder_adduct, elbow_flex]
