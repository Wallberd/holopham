import myo
import udpclient as udpc
import listeners as lsnter
import time
import threading
import IMUtJA2 as im2ja

FOREARM_MYO = "ThakorLab2"
SHOULDER_MYO = "Thakor Lab #5"

# Update the client class
def update_send_data(client, angles, reader):
    while True:
        client.set_data_array(angles.calc() + [reader.get_pose(FOREARM_MYO)])

# Continually poll Myos
def run_myos():
    while not stop:
        hub.run(reader.on_event, 500)

# Set up Myo library
myo.init(sdk_path='./myo-sdk-win-0.9.0/')
hub = myo.Hub()
reader = lsnter.listener(FOREARM_MYO, SHOULDER_MYO)

# Set up IMUtJA2
ja_calculator = im2ja.IMUtJA2("right", reader)

# Start client thread
client = udpc.udp_client("127.0.0.1", 65535)
client.start()

stop = False
x = ""

# Set up update threads
test = threading.Thread(target = update_send_data, args = (client, ja_calculator, reader))
test2 = threading.Thread(target = run_myos)
test2.start()

time.sleep(3)
while x != "q":
    x = input()

    # Start updating angles when calibration is complete
    if(x == "calibrate"):
        ja_calculator.calibrate()
        test.start()
    if (x == "quit"):
        break
    print(reader.get_pose(FOREARM_MYO))
stop = True
# for i in range(1200):
#     time.sleep(0.0166666667)
#     client.set_data_array([1, 2, 3, 4, 5, 6, 7])

hub.stop()
client.stop()
