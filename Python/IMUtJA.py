#TODO: clean up imports
import sys, path 

sys.path.insert(0,'../../')
sys.path.insert(0,'../../vMPL_Control')


import numpy as np
from mite.utils import Quaternion as quat
from mpl import JointEnum as MplId

import math, time, random, pylab
import matplotlib.pyplot as plt

""" IMU to Joint Angles 
"""
class IMUtJA: 
    def __init__(self, handedness): 
        """ Constructor """
        self.e1 = [0,0,0];
        self.e2 = [0,0,0];
        self.e3 = [0,0,0];
        if handedness == 'left':
            self.arm_mult = -1
        else:
            self.arm_mult = 1
    #def __del__(self):
        #"""Destructor """
    def calc(self,state):
        """ 
        calculate joint angles based on IMU quaternions
        state = [q3,q2,q1] from wrist, forarem, shoulder IMUs
        """
        angles = []
        q3 = state[0:4]
        q2 = state[4:8]
        q1 = state[8:12]

        # convert to euler angles 
        angles.append(quat.quaternion_to_euler_angle(q1))
        angles.append(quat.quaternion_to_euler_angle(q2))
        angles.append(quat.quaternion_to_euler_angle(q3))

        
        # maintain angle coninuity of polarity 
        # E1
        if (abs(angles[0][2]) > 1):
            if(np.sign(angles[0][2]) != np.sign(self.e1[2])):
                angles[0] =[angles[0][0], angles[0][1], angles[0][2]+ np.sign(self.e1[2])*2*math.pi]

        if (abs(angles[0][0]) > 1):
            if(np.sign(angles[0][0]) != np.sign(self.e1[0])):
                angles[0] =[angles[0][0] + np.sign(self.e1[1])*2*math.pi, angles[0][1], angles[0][2]]
        self.e1 = angles[0]

        # E2
        if (abs(angles[1][0]) > 1):
            if(np.sign(angles[1][0]) != np.sign(self.e2[0])):
                angles[1] =[angles[1][0]+np.sign(self.e2[0])*2*math.pi, angles[1][1], angles[1][2]]
        if (abs(angles[1][1]) > 1):
            if(np.sign(angles[1][1]) != np.sign(self.e2[1])):
                angles[1] =[angles[1][0], angles[1][1] + np.sign(old_q2_euler[1])*2*math.pi, angles[1][2]]
        self.e2  = angles[1]

        # E3
        self.e3 = angles[2]
       

        # inital values 
        shoulder_flex = 0
        shoulder_adduct = 0
        humeral_rotate = 0
        elbow_flex = 0
        wrist_rotate = 0
        wrist_flex = 0
        wrist_adduct = 0
        hand_flex = 0


        # add imu information
        shoulder_flex = angles[0][2]
        shoulder_adduct =-1* angles[0][0]
        elbow_flex = -1*(angles[1][0] + angles[0][2])
        wrist_rotate = ( angles[2][0])
#        wrist_flex = -1*(angles[2][2] + angles[1][0])


        # translate into 27 vMPL arm  angles
        arm_angles = [0.0] * 27
        arm_angles[MplId.SHOULDER_FE] = shoulder_flex
        arm_angles[MplId.SHOULDER_AB_AD] = shoulder_adduct
        arm_angles[MplId.HUMERAL_ROT] = humeral_rotate
        arm_angles[MplId.ELBOW] = elbow_flex
        arm_angles[MplId.WRIST_ROT] = wrist_rotate
        arm_angles[MplId.WRIST_FE] = wrist_flex
        arm_angles[MplId.WRIST_AB_AD] = wrist_adduct
        hand_idx = [MplId.INDEX_MCP, MplId.MIDDLE_MCP, MplId.RING_MCP, MplId.LITTLE_MCP]
        pip_idx = [MplId.INDEX_PIP, MplId.MIDDLE_PIP, MplId.RING_PIP, MplId.LITTLE_PIP, MplId.INDEX_DIP, MplId.MIDDLE_DIP, MplId.RING_DIP, MplId.LITTLE_DIP]
        thumb_idx = [MplId.THUMB_CMC_FE, MplId.THUMB_MCP, MplId.THUMB_DIP]
        arm_angles[MplId.THUMB_CMC_AB_AD] = 0.5 * hand_flex
        for idx in hand_idx:
            arm_angles[idx] = hand_flex
        for idx in pip_idx:
            arm_angles[idx] = 1.0 * hand_flex
        for idx in thumb_idx:
            arm_angles[idx] = 0.6 * hand_flex

        return arm_angles
