import socket
import threading
import struct
import time

# UDP client class
# Members:
#   dest - Destination IP of the server
#   port - Destination port of the server
#   data_packed - Packed version of the data. Currently set to 3 floats
#   is_running - State of the client
class udp_client:
    def __init__(self, dest, port):
        data = [1,0,0,0,0,0,0,0]
        self.__dest = dest
        self.__port = port
        self.__data_packed = struct.pack(f"<{len(data)-1}f1i", *data)
        self.__is_running = False

    # Continually send data to the server
    def __run(self):
        while self.__is_running:
            time.sleep(0.0166666667)
            try:
                self.__s.sendto(self.__data_packed, (self.__dest, self.__port-1))
            except:
                print("Failed to send data!")
        self.__s.close()

    # Set the state of the server to stop
    def stop(self):
        self.__is_running = False

    # Set the data to be sent to the server
    # Parameters:
    #   data: Array of floats to be sent
    def set_data_array(self, data):
        self.__data_packed = struct.pack(f"<{len(data)-1}f1i", *data)

    # Creates a thread to send data
    def start(self):
        if not self.__is_running:
            self.__init_socket()
            self.__thread = threading.Thread(target = self.__run)
            self.__thread.start()
        else:
            print("Client is already running!")

    # Initializes the socket
    def __init_socket(self):
        self.__s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__s.bind(("", self.__port))
        self.__is_running = True
